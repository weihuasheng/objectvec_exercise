//
//  ViewController.m
//  实现缩放
//
//  Created by 魏华生 on 2020/2/9.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.scrollView.contentSize=self.img.frame.size;//可以不要
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.showsVerticalScrollIndicator=NO;
    
    self.scrollView.delegate=self;
    
    self.scrollView.maximumZoomScale=3.5;
    self.scrollView.minimumZoomScale=0.5;
}

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    NSLog(@"ssss");
    return self.img;
}

@end
