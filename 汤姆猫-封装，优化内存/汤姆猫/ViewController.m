//
//  ViewController.m
//  汤姆猫
//
//  Created by 魏华生 on 2020/1/28.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)drink;
- (IBAction)scrate;
- (IBAction)eat;
- (IBAction)fart;
- (IBAction)cymbal;
- (IBAction)pie;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)drink {
   [self setAnimation:81 photoName:@"drink"];
}

- (IBAction)scrate {
 [self setAnimation:56 photoName:@"scratch"];
}

- (IBAction)eat {
 [self setAnimation:40 photoName:@"eat"];
}

- (IBAction)fart {
 [self setAnimation:28 photoName:@"fart"];

}

- (IBAction)cymbal {
    [self setAnimation:13 photoName:@"cymbal"];

}

- (IBAction)pie {
    [self setAnimation:24 photoName:@"pie"];

}

-(void)setAnimation:(int)times photoName:(NSString *)photoName{
    if(self.imgView.isAnimating){
        return ;
    }//保证按其他按钮不会影响当前按钮
    
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<times;i++){
        //消耗内存
//        NSString *imgName=[NSString stringWithFormat:@"%@_%02d",photoName,i];
//        UIImage *img=[UIImage imageNamed:imgName];
//        [arrayM addObject:img];
        NSString *imgName=[NSString stringWithFormat:@"%@_%02d.jpg",photoName,i];
        NSString *path=[[NSBundle mainBundle] pathForResource:imgName ofType:nil];
        UIImage *img=[UIImage imageWithContentsOfFile:path];
        [arrayM addObject:img];
    }
    self.imgView.animationImages=arrayM;
    self.imgView.animationDuration=arrayM.count*0.1;
    self.imgView.animationRepeatCount=1;
    [self.imgView startAnimating];
    
}
@end
