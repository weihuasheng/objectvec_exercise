//
//  WHSFlagView.m
//  UI进阶_UIPickerView_国旗选择
//
//  Created by weihuasheng on 2020/7/30.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "WHSFlagView.h"
#import "WHSFlag.h"
@interface WHSFlagView ()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgName;


@end

@implementation WHSFlagView
- (void)setFlag:(WHSFlag *)flag{
    _flag = flag;
    self.lblTitle.text = flag.name;
    self.imgName.image = [UIImage imageNamed:flag.icon];
    
}

+ (instancetype)flagView{
    return [[[NSBundle mainBundle] loadNibNamed:@"WHSFlagView" owner:nil options:nil] firstObject];
}

+ (CGFloat)rowHeight{
    return 90;
}



@end
