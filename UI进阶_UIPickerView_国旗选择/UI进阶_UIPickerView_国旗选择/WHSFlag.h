//
//  WHSFlag.h
//  UI进阶_UIPickerView_国旗选择
//
//  Created by weihuasheng on 2020/7/30.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSFlag : NSObject
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *icon;

-(instancetype)initWithDict:(NSDictionary *)dict;
+(instancetype)flagWithDict:(NSDictionary *)dict;


@end

NS_ASSUME_NONNULL_END
