//
//  WHSFlagView.h
//  UI进阶_UIPickerView_国旗选择
//
//  Created by weihuasheng on 2020/7/30.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN


@class WHSFlag;
@interface WHSFlagView : UIView
@property (nonatomic, strong) WHSFlag *flag;

+ (instancetype) flagView;
+ (CGFloat) rowHeight;

@end

NS_ASSUME_NONNULL_END
