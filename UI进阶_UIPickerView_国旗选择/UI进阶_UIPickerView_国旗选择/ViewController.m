//
//  ViewController.m
//  UI进阶_UIPickerView_国旗选择
//
//  Created by weihuasheng on 2020/7/30.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "ViewController.h"
#import "WHSFlag.h"
#import "WHSFlagView.h"

@interface ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic,strong) NSArray *flags;

@end

@implementation ViewController

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.flags.count;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    WHSFlagView *flagView = [WHSFlagView flagView];
    flagView.frame = CGRectMake(0, 20, self.view.bounds.size.width, [WHSFlagView rowHeight]);
    [flagView setFlag:self.flags[row]];
    return flagView;
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return [WHSFlagView rowHeight];
}


- (NSArray *)flags{
    if (_flags == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"03flags.plist" ofType:nil];
        NSArray *flagsArr = [NSArray arrayWithContentsOfFile:path];
        NSMutableArray *temp = [NSMutableArray array];
        for (NSDictionary *dict in flagsArr) {
            WHSFlag *flag = [WHSFlag flagWithDict:dict];
            [temp addObject:flag];
        }
        _flags = temp;
        
    }
    return _flags;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
