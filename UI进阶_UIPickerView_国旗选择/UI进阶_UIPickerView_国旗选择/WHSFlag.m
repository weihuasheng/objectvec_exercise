//
//  WHSFlag.m
//  UI进阶_UIPickerView_国旗选择
//
//  Created by weihuasheng on 2020/7/30.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "WHSFlag.h"

@implementation WHSFlag
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)flagWithDict:(NSDictionary *)dict{
    return [[self alloc]initWithDict:dict];
}

@end
