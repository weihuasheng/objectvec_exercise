//
//  WHSShopingFooterView.h
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/17.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol WHSShopingFooterViewDelegate <NSObject>
@required
- (void)shopingFooterViewUpdeteData;

@end
@interface WHSShopingFooterView : UIView
+ (instancetype)shoppingFooterView;
@property (nonatomic,weak) id<WHSShopingFooterViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
