//
//  WHSShopingHeaderVew.m
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/22.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSShopingHeaderVew.h"
@interface WHSShopingHeaderVew()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;
@property (nonatomic,strong) NSTimer *timer;

@end

@implementation WHSShopingHeaderVew

+ (instancetype) shopingHeaderView{
    return [[[NSBundle mainBundle]loadNibNamed:@"WHSShopingHeaderView" owner:nil options:nil]firstObject];
}

- (void)awakeFromNib{
    int numberOfPic = 5;
    CGFloat imgW = 320;
    CGFloat imgH = 128;
    for(int i=0;i<numberOfPic;i++){
        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ad_%02d",i]];
        CGFloat imgY = 0;
        CGFloat imgX = i*imgW;
        imgView.frame=CGRectMake(imgX, imgY, imgW, imgH);
        [self.scrollView addSubview:imgView];
        
    }
    self.scrollView.contentSize=CGSizeMake(numberOfPic*imgW,0);
    self.scrollView.pagingEnabled=YES;
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.pageControll.numberOfPages=numberOfPic;
    self.pageControll.currentPage=0;
    self.scrollView.delegate=self;
    self.timer=[NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSInteger page=self.pageControll.currentPage;
        if (page==self.pageControll.numberOfPages-1) {
            page=0;
        }else{
            page++;
        }
        CGFloat offsetX=page*self.scrollView.frame.size.width;
        [self.scrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
    }];
    NSRunLoop *runLop = [NSRunLoop currentRunLoop];
    [runLop addTimer:self.timer forMode:NSRunLoopCommonModes];
    
    
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offSetX=scrollView.contentOffset.x+scrollView.frame.size.width*0.5;
    int pageNo=offSetX/scrollView.frame.size.width;
    self.pageControll.currentPage=pageNo;
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.timer invalidate];//停止计时器
    self.timer=nil;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSInteger page=self.pageControll.currentPage;
        if (page==self.pageControll.numberOfPages-1) {
            page=0;
        }else{
            page++;
        }
        CGFloat offsetX=page*self.scrollView.frame.size.width;
        [self.scrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
    }];
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
    [runLoop addTimer:self.timer forMode:NSRunLoopCommonModes];
}
@end
