//
//  ViewController.m
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/16.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSShopingCell.h"
#import "WHSShopingFooterView.h"
#import "WHSShopingHeaderVew.h"
@interface ViewController ()<UITableViewDataSource,WHSShopingFooterViewDelegate>
@property (nonatomic, strong) NSMutableArray *shops;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ViewController 
- (NSArray *)shops{
    if(_shops==nil){
        NSString *path=[[NSBundle mainBundle]pathForResource:@"tgs.plist" ofType:nil];
        NSArray *array=[NSArray arrayWithContentsOfFile:path];
        NSMutableArray *arrayModel=[NSMutableArray array];
        for (NSDictionary*dict in array) {
            WHSShoping* model=[WHSShoping shopingWithDict:dict];
            [arrayModel addObject:model];
        }
        _shops=arrayModel;
    }
    return _shops;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.dataSource=self;
    WHSShopingFooterView *footerView = [WHSShopingFooterView shoppingFooterView];
    WHSShopingHeaderVew *headerView = [WHSShopingHeaderVew shopingHeaderView];
    footerView.delegate = self;
    
    
    
    self.tableView.tableFooterView = footerView;
    self.tableView.tableHeaderView = headerView;
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.shops.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WHSShoping *model=self.shops[indexPath.row];
    //创建自定义cell
    WHSShopingCell*cell=[WHSShopingCell shopsCellWithTableView:tableView];
    //给cell里的控件赋值
    cell.shops=model;
    
    return cell;
}

-(void)shopingFooterViewUpdeteData{
    //创建一个模型对象
    WHSShoping *model=[[WHSShoping alloc]init];
    model.title=@"驴打滚";
    model.icon=@"2c97690e72365e38e3e2a95b934b8dd2";
    model.price=@"125";
    model.buyCount=@"650";
    //把模型对象加到模型中
    [self.shops addObject:model];
    //刷新UITableView
    [self.tableView reloadData];
    //    // 局部刷新(只适用于UITableView总行数没有发生变化的情况)
    //    NSIndexPath *idxPath = [NSIndexPath indexPathForRow:self.goods.count - 1 inSection:0];
    //    [self.tableView reloadRowsAtIndexPaths:@[idxPath] withRowAnimation:UITableViewRowAnimationLeft];
   
    
    //把UITableView的最后一行的数据滚动到最上面
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.shops.count-1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (BOOL)prefersStatusBarHidden{
    return YES;
}

@end
