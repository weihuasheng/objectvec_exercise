//
//  WHSShoping.h
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/16.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSShoping : NSObject
@property(nonatomic,copy)NSString* buyCount;
@property(nonatomic,copy)NSString* icon;
@property(nonatomic,copy)NSString* price;
@property(nonatomic,copy)NSString* title;

-(instancetype)initWithDict:(NSDictionary*)dict;
+(instancetype)shopingWithDict:(NSDictionary*)dict;
@end

NS_ASSUME_NONNULL_END
