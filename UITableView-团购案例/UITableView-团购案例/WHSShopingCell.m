//
//  WHSShopingCell.m
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/16.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSShopingCell.h"
#import "WHSShoping.h"
@interface WHSShopingCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyCountLabel;

@end
@implementation WHSShopingCell
- (void)setShops:(WHSShoping *)shops{
    _shops=shops;
    self.imageIcon.image=[UIImage imageNamed:shops.icon];
    self.labelTitle.text=shops.title;
    self.priceLabel.text=[NSString stringWithFormat:@"💰 %@",shops.price];
    self.buyCountLabel.text=[NSString stringWithFormat:@"%@人已购买",shops.buyCount ];
    
}
+(instancetype)shopsCellWithTableView:(UITableView *)tableView{
    static NSString* ID=@"shops_cell";
    WHSShopingCell* cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if(cell==nil){
        cell=[[[NSBundle mainBundle]loadNibNamed:@"WHSShopingCell" owner:nil options:nil]firstObject];
    }
    return cell;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
