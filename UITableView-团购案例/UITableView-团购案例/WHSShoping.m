//
//  WHSShoping.m
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/16.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSShoping.h"

@implementation WHSShoping
- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self=[super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)shopingWithDict:(id)dict{
    return [[self alloc]initWithDict:dict];
}
@end
