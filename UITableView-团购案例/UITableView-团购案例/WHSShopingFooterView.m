//
//  WHSShopingFooterView.m
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/17.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSShopingFooterView.h"

@interface WHSShopingFooterView()
@property (weak, nonatomic) IBOutlet UIButton *btnDowmload;
@property (weak, nonatomic) IBOutlet UIView *viewLoad;
- (IBAction)btnDomwloadClick;



@end

@implementation WHSShopingFooterView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (instancetype)shoppingFooterView{
    WHSShopingFooterView *footerView=[[[NSBundle mainBundle]loadNibNamed:@"WHSShopingFooterView" owner:nil options:nil]firstObject];
    return footerView;
}

- (IBAction)btnDomwloadClick {
    
    self.btnDowmload.hidden=YES;
    self.viewLoad.hidden=NO;
    //1.0秒后执行
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //调用代理方法实现下面的功能
        //调用ShopingFooterViewUpdateData方法之前，为了保证调用不出错，
        //所以要判断一下代理对象是否真的实现了这个方法，如果实现了这个方法再调用
        if([self.delegate respondsToSelector:@selector(shopingFooterViewUpdeteData)]){
            [self.delegate shopingFooterViewUpdeteData];
        }
        self.btnDowmload.hidden = NO;
        self.viewLoad.hidden    = YES;
            
    });
    
}

@end
