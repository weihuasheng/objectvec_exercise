//
//  WHSShopingHeaderVew.h
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/22.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSShopingHeaderVew : UIView
+ (instancetype) shopingHeaderView;

@end

NS_ASSUME_NONNULL_END
