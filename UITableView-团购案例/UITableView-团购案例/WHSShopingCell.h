//
//  WHSShopingCell.h
//  UITableView-团购案例
//
//  Created by 魏华生 on 2020/2/16.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class WHSShoping;
@interface WHSShopingCell : UITableViewCell
@property(nonatomic,strong)WHSShoping *shops;
+(instancetype)shopsCellWithTableView:(UITableView*)tableView;
@end

NS_ASSUME_NONNULL_END
