//
//  ViewController.h
//  Testing
//
//  Created by student_weihuasheng on 2019/11/14.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WHSTextField.h"


@interface ViewController : UIViewController<UITextFieldDelegate>

@property(nonatomic,strong) UIImageView* ig;
@property(nonatomic,strong) UILabel* label;
@property(nonatomic,strong) WHSTextField* loginText;
@property(nonatomic,strong) WHSTextField* passwordText;
@property(nonatomic,strong) UIButton* button;
@property(nonatomic,strong) UILabel* label1;







@end

