//
//  ViewController.m
//  Testing
//
//  Created by student_weihuasheng on 2019/11/14.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import "ViewController.h"
#import "baseUITextField.h"

@interface ViewController ()

@end

@implementation ViewController
  

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat height_photo=1330.0;//页面标注的高
    CGFloat width_photo=750.0;//页面标注的宽
    CGFloat width=self.view.frame.size.width;//手机的宽度
    CGFloat height=self.view.frame.size.height;//手机的高
    
    //图片
    CGFloat width_image=750.0;//放入图片的宽
    CGFloat height_image=452.0;//放入图片的高
    CGFloat imageX=0.0;
    CGFloat imageY=0.0;
    double imageH=(height_image/height_photo)*height;
    double imaghW=width;
    _ig=[[UIImageView alloc]initWithFrame:CGRectMake(imageX, imageY, imaghW, imageH)];
    [_ig setImage:[UIImage imageNamed:[[NSString alloc] initWithFormat:@"login_bg"]]];
    [self.view addSubview:_ig];
    
    //sign in
    CGFloat labelX=(323.0/width_image)*width;
    CGFloat labelY=imageH+(46.0/width_photo)*height;
    double width_label=(104.0/width_photo)*width;
    double height_label=(34.0/height_photo)*height;
    _label=[[UILabel alloc]initWithFrame:CGRectMake(labelX , labelY, width_label, height_label)];
    _label.text=@"Sign in";
    _label.textColor=[UIColor colorWithRed:142.0/255.0 green:191.0/255.0 blue:47.0/255.0 alpha:1.0];
    _label.font=[UIFont systemFontOfSize:16];
    [self.view addSubview:_label];
    
    
    //学号
    CGFloat textX=(84.0/width_photo)*width;
    CGFloat textY=(48.0/height_photo)*height+labelY+height_label;
    double textwidth=(582.0/width_photo)*width;
    double textHeight=(100.0/height_photo)*height;
    _loginText=[[WHSTextField alloc]initWithFrame:CGRectMake(textX,textY,textwidth,textHeight)];
    _loginText.borderStyle=UITextBorderStyleRoundedRect;
    _loginText.placeholder=@"学号";
    _loginText.font=[UIFont systemFontOfSize:13];
    
   CGFloat loginImageX=(18.0/width_photo)*_loginText.frame.size.width;
    CGFloat loginImageY=(27.0/height_photo)*_loginText.frame.size.height;
    double loginHeight=(44.0/height_photo)*_loginText.frame.size.height;
    double loginWidth=(44.0/width_photo)*_loginText.frame.size.width;
    //UIImageView *loginImage=[[UIImageView alloc]initWithFrame:CGRectMake(loginImageX+10, loginImageY, loginWidth, loginHeight)];
    UIImageView *loginImage=[[UIImageView alloc]init];
    loginImage.image=[UIImage imageNamed:@"icon_people"];
    _loginText.leftView=loginImage;
    _loginText.leftViewMode=UITextFieldViewModeAlways;
    _loginText.delegate=self;
    _loginText.clearButtonMode = UITextFieldViewModeAlways;
    [_loginText addTarget:self action:@selector(listenTextField:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:_loginText];
    
 
    
    
    //密码
    CGFloat text1X=textX;
    CGFloat text1Y=(62.0/height_photo)*height+textY+textHeight;
    double text1Width=textwidth;
    double text1Height=textHeight;
    _passwordText=[[WHSTextField alloc]initWithFrame:CGRectMake(text1X,text1Y,text1Width, text1Height)];
    _passwordText.borderStyle=UITextBorderStyleRoundedRect;
    _passwordText.placeholder=@"密码（初始密码为身份证后六位）";
    _passwordText.font=[UIFont systemFontOfSize:13];
    CGFloat passImageX=loginImageX;
    CGFloat passImageY=loginImageY;
    double passHeight=loginHeight;
    double passWidth=loginWidth;
    UIImageView *passImage=[[UIImageView alloc]initWithFrame:CGRectMake(passImageX, passImageY, passWidth, passHeight)];
    passImage.image=[UIImage imageNamed:@"icon_lock"];
    _passwordText.leftView=passImage;
    _passwordText.leftViewMode=UITextFieldViewModeAlways;
    _passwordText.delegate=self;
    _passwordText.secureTextEntry=YES;
    [_passwordText addTarget:self action:@selector(listenTextField:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addSubview:_passwordText];
  
    //登陆
    CGFloat buttonX=(166.0/width_photo)*width;
    CGFloat buttonY=(85.0/height_photo)*height+text1Y+text1Height;
    double buttonWidth=(417.0/width_photo)*width;
    double buttonHeight=(102.0/height_photo)*height;
    _button=[[UIButton alloc]initWithFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight)];
    [_button setTitle:@"登陆" forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(setLoginBtnState:) forControlEvents:UIControlEventTouchUpInside];
    [_button setBackgroundImage:[UIImage imageNamed:@"button_dis"] forState:UIControlStateNormal];
    [self.view addSubview:_button];
    
    
    //忘记密码
    double labelWidth1=(104.0/width_photo)*width;
    double labelHeight1=(24.0/height_photo)*height;
    CGFloat labelX1=width-(84.0/width_photo)*width-labelWidth1;
    CGFloat labelY1=height-(225.0/height_photo)*height-labelHeight1;
    _label1=[[UILabel alloc]initWithFrame:CGRectMake(labelX1, labelY1, labelWidth1, labelHeight1)];
    _label1.text=@"忘记密码";
    _label1.font=[UIFont systemFontOfSize:10];
    _label1.textColor=[UIColor colorWithRed:142.0/255.0 green:191.0/255.0 blue:47.0/255.0 alpha:1.0];
    _label1.userInteractionEnabled=YES;
    UITapGestureRecognizer *test=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelTouch)];
    [_label1 addGestureRecognizer:test];
    
    [self.view addSubview:_label1];
    


}

- (void)btnClick {
    NSLog(@"登陆");
}

-(void)labelClick{
    NSLog(@"忘记密码");
}

-(void)labelTouch{
    NSLog(@"忘记密码");
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
   CGRect frame = textField.frame;
    CGFloat heights = self.view.frame.size.height;
    // 当前点击textfield的坐标的Y值 + 当前点击textFiled的高度 - （屏幕高度- 键盘高度 - 键盘上tabbar高度）
    // 在这一部 就是了一个 当前textfile的的最大Y值 和 键盘的最全高度的差值，用来计算整个view的偏移量
    int offset = frame.origin.y + 200- ( heights - 216.0-60.0);//键盘高度216
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyBoard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    double width = self.view.frame.size.width;
    double height = self.view.frame.size.height;
    if(offset > 0)
    {
        CGRect rect = CGRectMake(0.0, -offset,width,height);
        self.view.frame = rect;
        }
        [UIView commitAnimations];
}

- (void)listenTextField:(UITextField*)tf
{
    if (self.loginText.text.length > 0 && self.passwordText.text.length > 0) {
        [self setLoginBtnState:YES];
    }
    else {
        [self setLoginBtnState:NO];
    }
}
 
- (void)setLoginBtnState:(BOOL)canClick
{
    if (canClick) {
        self.button.userInteractionEnabled = YES;
         [_button setBackgroundImage:[UIImage imageNamed:@"button_pre"] forState:UIControlStateNormal];
      [self.button addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        self.button.userInteractionEnabled = NO;
            [self.button setBackgroundImage:[UIImage imageNamed:@"button_dis"] forState:UIControlStateNormal];
    }
   



}
@end
