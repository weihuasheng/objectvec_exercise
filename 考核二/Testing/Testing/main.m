//
//  main.m
//  Testing
//
//  Created by student_weihuasheng on 2019/11/14.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ViewController.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
    }
      
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
