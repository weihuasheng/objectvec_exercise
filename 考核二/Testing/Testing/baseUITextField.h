//
//  baseUITextField.h
//  Testing
//
//  Created by student_weihuasheng on 2019/11/17.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface baseUITextField : UITextField
-(CGRect)leftViewRectForBounds:(CGRect)bounds;
@end

NS_ASSUME_NONNULL_END
