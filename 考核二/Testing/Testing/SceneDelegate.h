//
//  SceneDelegate.h
//  Testing
//
//  Created by student_weihuasheng on 2019/11/14.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

