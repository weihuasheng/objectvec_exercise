//
//  WHSApp.m
//  UITableView-应用管理器
//
//  Created by weihuasheng on 2020/4/4.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "WHSApp.h"

@implementation WHSApp

- (instancetype)initWithDict:(NSDictionary *)dict{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)appWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}


@end
