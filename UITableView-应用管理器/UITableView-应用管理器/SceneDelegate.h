//
//  SceneDelegate.h
//  UITableView-应用管理器
//
//  Created by weihuasheng on 2020/4/4.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

