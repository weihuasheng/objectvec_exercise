//
//  WHSApp.h
//  UITableView-应用管理器
//
//  Created by weihuasheng on 2020/4/4.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSApp : NSObject
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *dowmload;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon;

- (instancetype)initWithDict: (NSDictionary *)dict;
+ (instancetype)appWithDict: (NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
