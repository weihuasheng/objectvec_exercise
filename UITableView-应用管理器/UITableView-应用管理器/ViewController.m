//
//  ViewController.m
//  UITableView-应用管理器
//
//  Created by weihuasheng on 2020/4/4.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "ViewController.h"
#import "WHSApp.h"

@interface ViewController ()

@property (nonatomic, strong)NSArray *apps;

@end

@implementation ViewController

#pragma mark -懒加载数据
- (NSArray *)apps{
    if (_apps == nil) {
        NSString *path = [[NSBundle mainBundle]pathForResource:@"apps_full.plist" ofType:nil];
        NSMutableArray *arrayM = [NSMutableArray array];
        NSArray *model = [NSArray arrayWithContentsOfFile:path];
        for (NSDictionary *dict in model) {
            WHSApp *app = [WHSApp appWithDict:dict];
            [arrayM addObject:app];
        }
        _apps = arrayM;
    }
    return _apps;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


@end
