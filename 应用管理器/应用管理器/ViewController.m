//
//  ViewController.m
//  应用管理器
//
//  Created by 魏华生 on 2020/1/30.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(nonatomic,strong) NSArray *pic;
@end

@implementation ViewController

//懒加载-重写get方法
- (NSArray *)pic{
    if(_pic==nil){
        NSString *path=[[NSBundle mainBundle] pathForResource:@"app.plist" ofType:nil];
        _pic=[NSArray arrayWithContentsOfFile:path];
    }
    return _pic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    int countX=3;//每行显示3个应用
    CGFloat viewWidth=self.view.frame.size.width;
    CGFloat picWidth=75;//每个view的长度
    CGFloat picHeight=90;//每个view的高度；
    CGFloat marginX=(viewWidth-picWidth*countX)/4.0;//view距离左边的边界
    CGFloat marginTop=45;//第一行view距离上边界的距离
    CGFloat picX;//每个view的x坐标
    CGFloat picY;//每个view的y坐标
    for (int i=0; i<self.pic.count; i++) {
          UIView *appView=[[UIView alloc] init];
          picX=marginX+(picWidth+marginX)*(i%countX);
          picY=marginTop+(picHeight+marginX)*(i/countX);
          appView.frame=CGRectMake(picX, picY, picWidth, picHeight);
          [self.view addSubview:appView];
        
        //添加子控件
        //添加图片框
        UIImageView *imageIcon=[[UIImageView alloc] init];
        CGFloat iconW=45;
        CGFloat iconH=45;
        CGFloat iconX=(appView.frame.size.width-iconW)/2;
        CGFloat iconY=0;
        imageIcon.frame=CGRectMake(iconX, iconY, iconW, iconH);
        [appView addSubview:imageIcon];
        
        //添加图片框数据
        NSDictionary *iconDic=self.pic[i];
        imageIcon.image=[UIImage imageNamed:iconDic[@"icon"]];
        
        //添加标题
        UILabel *label=[[UILabel alloc] init];
        CGFloat labelW=picWidth;
        CGFloat labelH=20;
        CGFloat labelX=0;
        CGFloat labelY=iconH;
        label.frame=CGRectMake(labelX, labelY, labelW, labelH);
        [appView addSubview:label];
        
        //添加标题数据
        label.text=iconDic[@"name"];
        label.font=[UIFont systemFontOfSize:12];
        label.textAlignment=NSTextAlignmentCenter ;
        
        //添加按钮
        UIButton *button=[[UIButton alloc]init];
        CGFloat btnW=iconW;
        CGFloat btnH=picHeight-iconH-labelH;
        CGFloat btnX=iconX;
        CGFloat btnY=iconH+labelH;
        button.frame=CGRectMake(btnX, btnY, btnW, btnH);
        [appView addSubview:button];
        
        //为按钮添加数据
        [button setTitle:@"下载" forState:UIControlStateNormal];
        [button setTitle:@"已安装" forState:UIControlStateDisabled];
        [button setBackgroundImage:[UIImage imageNamed:@"buttongreen"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"buttongreen_highlighted"] forState:UIControlStateDisabled];
        button.titleLabel.font=[UIFont systemFontOfSize:14];
        
        
    }
    
   
        
    
    
}



@end
