//
//  SceneDelegate.h
//  应用管理器
//
//  Created by 魏华生 on 2020/1/30.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

