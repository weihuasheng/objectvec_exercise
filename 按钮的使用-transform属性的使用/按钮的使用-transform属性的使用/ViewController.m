//
//  ViewController.m
//  按钮的使用-transform属性的使用
//
//  Created by 魏华生 on 2020/1/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *photo;
- (IBAction)move:(UIButton *)sender;
- (IBAction)scale:(UIButton *)sender;
- (IBAction)rotate:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)move:(UIButton *)sender {
    //只能改变一次
//    if(sender.tag==10){
//        self.photo.transform=CGAffineTransformMakeTranslation(0, -25);
//    }
//    else if(sender.tag==11){
//        self.photo.transform=CGAffineTransformMakeTranslation(25, 0);
//    }
//    else if(sender.tag==12){
//        self.photo.transform=CGAffineTransformMakeTranslation(0, 25);
//    }
//    else if(sender.tag==13){
//        self.photo.transform=CGAffineTransformMakeTranslation(-25, 0);
//    }
    if(sender.tag==10){
        [UIView animateWithDuration:1.5 animations:^{
            self.photo.transform=CGAffineTransformTranslate(self.photo.transform, 0, -25);
        }];
      }
      else if(sender.tag==11){
          [UIView animateWithDuration:1.5 animations:^{
              self.photo.transform=CGAffineTransformTranslate(self.photo.transform, 25, 0);
          }];
      }
      else if(sender.tag==12){
         [UIView animateWithDuration:1.5 animations:^{
              self.photo.transform=CGAffineTransformTranslate(self.photo.transform, 0, 25);
          }];
      }
      else if(sender.tag==13){
         [UIView animateWithDuration:1.5 animations:^{
             self.photo.transform=CGAffineTransformTranslate(self.photo.transform, -25, 0);
         }];
      }
}

- (IBAction)scale:(UIButton *)sender {
    if(sender.tag==14){
        [UIView animateWithDuration:1.5 animations:^{
            self.photo.transform=CGAffineTransformScale(self.photo.transform, 1.5, 1.5);
        }];
    }
    else if(sender.tag==15){
        [UIView animateWithDuration:1.5 animations:^{
            self.photo.transform=CGAffineTransformScale(self.photo.transform, 0.5, 0.5);
        }];
    }
}

- (IBAction)rotate:(UIButton *)sender {
    if(sender.tag==16){
        [UIView animateWithDuration:1.5 animations:^{
            self.photo.transform=CGAffineTransformRotate(self.photo.transform,M_PI_4);
        }];
    }
    if(sender.tag==17){
           [UIView animateWithDuration:1.5 animations:^{
               self.photo.transform=CGAffineTransformRotate(self.photo.transform,-M_PI_4);
           }];
       }
}
@end
