//
//  SceneDelegate.h
//  按钮的使用-transform属性的使用
//
//  Created by 魏华生 on 2020/1/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

