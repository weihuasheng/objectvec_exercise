//
//  ViewController.m
//  按钮的使用-动画的实现
//
//  Created by 魏华生 on 2020/1/24.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *page;
- (IBAction)down;
- (IBAction)plus;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)down {
   // CGRect originFrame=self.page.frame;
    //originFrame.origin.y +=100;//增大移动的距离，发现移动的速度太快
    
    //动画的使用
    //开启动画
    [UIView animateWithDuration:2 animations:^{
        CGRect originFrame=self.page.frame;
        originFrame.origin.y += 100;
        self.page.frame=originFrame;
    }];
    //self.page.frame=originFrame;
}

- (IBAction)plus {
    CGRect originFrame=self.page.frame;
    originFrame.size.width +=100;
    originFrame.size.height +=100;
    self.page.frame=originFrame;
}
@end
