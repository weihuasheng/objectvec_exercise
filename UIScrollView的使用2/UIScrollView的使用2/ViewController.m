//
//  ViewController.m
//  UIScrollView的使用2
//
//  Created by 魏华生 on 2020/2/7.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)scroll;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.scrollView.contentSize=self.imgView.image.size;
  
}


- (IBAction)scroll {
    CGPoint point=self.scrollView.contentOffset;
    point.x +=25;
    point.y +=25;
//    //self.scrollView.contentOffset=point;没有动画效果
//    [UIView animateWithDuration:1.0 animations:^{
//        self.scrollView.contentOffset=point;
//    }];
    //还有一种便捷的方法
    [self.scrollView setContentOffset:point animated:YES];
}
@end
