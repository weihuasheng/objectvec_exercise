//
//  ViewController.m
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/2/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSMessageFrame.h"
#import "WHSMessage.h"
#import "WHSMessageCell.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtInput;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *messageFrames;

@end

@implementation ViewController

- (NSMutableArray *)messageFrames{
    if(_messageFrames == nil){
        NSString *path = [[NSBundle mainBundle] pathForResource:@"messages.plist" ofType:nil];
        NSMutableArray *arrayM = [NSMutableArray array];
        NSArray *model = [NSArray arrayWithContentsOfFile:path];
        for(NSDictionary *dict in model){
            //创建一个数据模型
            WHSMessage *model = [WHSMessage messageWithDict:dict];
            
            //获取上一个模型数据
            WHSMessage *lastMessage = (WHSMessage *)[[arrayM lastObject] message];
            
            //判断当前模型的“消息发送时间”是否和上一个模型的“消息发送时间”一致，如果一致做个标记
            if ([model.time isEqualToString:lastMessage.time]) {
                model.hideTime = YES;
            }
            
            //创建一个frame模型
            WHSMessageFrame *messageFrame =[[WHSMessageFrame alloc] init];
            messageFrame.message = model;
            [arrayM addObject:messageFrame];
        }
        _messageFrames = arrayM;
    }
    return _messageFrames;
}

//当键盘上的return键被单击的时候出发
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    //1. 获取用户输入的文本
    NSString *text = textField.text;
    
    //2.发送t用户的消息
    [self sendMessage:text withType:WHSMessageTypeMe];
    //3.发送一个系统消息
    [self sendMessage:@"我也爱你" withType:WHSMessageTypeother];
    
    //请空文本框
    textField.text = nil;
    return YES;
}

- (void)sendMessage: (NSString *)msg withType:(WHSMessageType)type{
    //1.创建一个数据模型和frame模型
    WHSMessage *model = [[WHSMessage alloc]init];
    
    //获取当前系统的时间
    NSDate *nowDate = [NSDate date];
    //创建一个日期时间格式化器
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //设置格式
    formatter.dateFormat = @"今天 HH:mm";
    //进行日期时间的格式化
    model.time = [formatter stringFromDate:nowDate];
    model.type = type;
    model.text = msg;
    
    
    
    //根据当前消息的时间和上一条消息的时间，来设置是否需要隐藏时间label
    WHSMessageFrame *lastMessageFrame = [self.messageFrames lastObject];
    NSString *lastTime = lastMessageFrame.message.time;
    if ([model.time isEqualToString:lastTime]) {
        model.hideTime = YES;
    }
    
    WHSMessageFrame *modelFrame = [[WHSMessageFrame alloc] init];
    modelFrame.message = model;
    
    //把frame加到集合中
    [self.messageFrames addObject:modelFrame];
    
    //刷新数据
    [self.tableView reloadData];
    
    //把最后一行滚动到最上面
    NSIndexPath *idcPath = [NSIndexPath indexPathForRow:self.messageFrames.count-1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:idcPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    //把键盘叫回去，思路：d让控制器所管理的UIView结束编辑
    [self.view endEditing:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messageFrames.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WHSMessageFrame *messageFrame1 = self.messageFrames[indexPath.row];
    WHSMessageCell *cell = [WHSMessageCell messageCellWithTableViwe:tableView];
    cell.messageFrame = messageFrame1;
    return cell;
}

// 返回每一行的行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    WHSMessageFrame *messageFrame = self.messageFrames[indexPath.row];
    return messageFrame.rowHeight;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.txtInput.delegate = self;
    
    //取消分割线
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //设置UITableView的背景色
    self.tableView.backgroundColor = [UIColor colorWithRed:236 / 255.0 green:236 /255.0 blue:236 / 255.0 alpha:1.0];
    
    // 设置UITableView的行不允许被选中
    self.tableView.allowsSelection = NO;
    
    //设置文本框最左侧有一段距离
    UIView *leftView = [[UIView alloc] init];
    leftView.frame = CGRectMake(0, 0, 5, 1);
    
    //把LeftView设置给文本框    self.txtInput.leftView = leftView;
    self.txtInput.leftViewMode = UITextFieldViewModeAlways;
    //监听键盘的弹出事件
      //1.创建一个NSNotifactionCenter对象
     NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    //2.监听键盘的弹出通知
    [center addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
}

- (void)keyboardWillChangeFrame:(NSNotification *)noteInfo{
     // 1. 获取当键盘显示完毕或者隐藏完毕后的Y值
    CGRect rectEnd = [noteInfo.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardY = rectEnd.origin.y;
    // 用键盘的Y值减去屏幕的高度计算出平移的值
    // 1. 如果是键盘弹出事件, 那么计算出的值就是负的键盘的高度
    // 2. 如果是键盘的隐藏事件, 那么计算出的值就是零， 因为键盘在隐藏以后, 键盘的Y值就等于屏幕的高度。
    CGFloat tranformValue = keyboardY - self.view.frame.size.height;
    [UIView animateWithDuration:0.25 animations:^{
        // 让控制器的View执行一次“平移”
        self.view.transform = CGAffineTransformMakeTranslation(0, tranformValue);
    }];
    
    
   //把最后一行滚动到最上面
   NSIndexPath *lastidxPath = [NSIndexPath indexPathForRow:self.messageFrames.count-1 inSection:0];
   [self.tableView scrollToRowAtIndexPath:lastidxPath atScrollPosition:UITableViewScrollPositionTop animated:YES];

}

//移除通知
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}




@end
