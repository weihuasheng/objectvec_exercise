//
//  NSString+NSString_WHSNSStringExt.m
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/3/3.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "NSString+NSString_WHSNSStringExt.h"



@implementation NSString (NSString_WHSNSStringExt)
- (CGSize)sizeOfTextWithMaxSize:(CGSize)maxsize font:(UIFont *)font{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [self boundingRectWithSize:maxsize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

+ (CGSize)sizeWithText:(NSString *)text maxsize:(CGSize)maxSize font:(UIFont *)font{
    return [text sizeOfTextWithMaxSize:maxSize font:font];
}
@end
