//
//  WHSMessage.h
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/2/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum{
    WHSMessageTypeMe = 0,
    WHSMessageTypeother = 1
}WHSMessageType;

@interface WHSMessage : NSObject
@property (nonatomic ,copy) NSString *text;
@property (nonatomic ,copy) NSString *time;
//消息的类型
@property (nonatomic ,assign) WHSMessageType type;
@property (nonatomic ,assign) BOOL hideTime;
- (instancetype) initWithDict: (NSDictionary *)dict;
+ (instancetype) messageWithDict: (NSDictionary *)dict;


@end

NS_ASSUME_NONNULL_END
