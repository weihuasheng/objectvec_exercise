//
//  WHSMessageFrame.h
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/2/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WHSMessage.h"
#import <CoreGraphics/CoreGraphics.h>
#define textFont [UIFont systemFontOfSize:13]

NS_ASSUME_NONNULL_BEGIN
@class WHSMessage;
@interface WHSMessageFrame : NSObject
@property (nonatomic ,strong) WHSMessage *message;
@property (nonatomic ,assign ,readonly) CGRect timeFrame;
@property (nonatomic ,assign ,readonly) CGRect iconFrame;
@property (nonatomic ,assign ,readonly) CGRect messageFrame;
@property (nonatomic ,assign ,readonly) CGFloat rowHeight;


@end

NS_ASSUME_NONNULL_END
