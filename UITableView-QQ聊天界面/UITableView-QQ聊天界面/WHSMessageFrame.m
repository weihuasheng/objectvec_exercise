//
//  WHSMessageFrame.m
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/2/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSMessageFrame.h"
#import "NSString+NSString_WHSNSStringExt.h"

@implementation WHSMessageFrame

- (void)setMessage:(WHSMessage *)message{
    _message = message;
    //设置坐标和行高
    //获取屏幕宽度
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    //设置统一的间距
    CGFloat margin = 5;
    
    //计算时间label的frame
    CGFloat timeX = 0;
    CGFloat timeY = 0;
    CGFloat timeW = screenW;
    CGFloat timeH = 15;
    if(!message.hideTime){
        //如果需要显示时间label，那么在计算时间label的frame
        _timeFrame = CGRectMake(timeX, timeY, timeW, timeH);
    }
    
    //计算头像的frame
    CGFloat iconW = 30;
    CGFloat iconH = 30;
    CGFloat iconY = CGRectGetMaxY(_timeFrame) + margin;
    CGFloat iconX = message.type == WHSMessageTypeother ?margin: screenW - margin - iconW;
    _iconFrame = CGRectMake(iconX, iconY, iconW, iconH);
    
    //计算消息正文的frame
    //1.先计算正文的大小
    CGSize textSize = [message.text sizeOfTextWithMaxSize:CGSizeMake(200, MAXFLOAT) font:textFont];
    CGFloat textW = textSize.width + 40;
    CGFloat textH = textSize.height + 30;
    //2.再计算x，y
    CGFloat textY = iconY;
    CGFloat textX = message.type == WHSMessageTypeother ? CGRectGetMaxX(_iconFrame) : (screenW - margin - iconW - textW);
    _messageFrame = CGRectMake(textX, textY, textW, textH);
    
    
    //计算行高
    //获取头像的最大的y值和正文的最大的y值，然后用最大的y值+margin
    CGFloat maxY = MAX(CGRectGetMaxY(_messageFrame), CGRectGetMaxY(_iconFrame));
    _rowHeight = maxY + margin;
    
    
    
    
}
@end
