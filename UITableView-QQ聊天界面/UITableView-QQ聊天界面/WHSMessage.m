//
//  WHSMessage.m
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/2/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSMessage.h"

@implementation WHSMessage



- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self =[super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)messageWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}

@end
