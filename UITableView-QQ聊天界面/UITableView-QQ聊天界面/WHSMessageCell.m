//
//  WHSMessageCell.m
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/2/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSMessageCell.h"

@interface WHSMessageCell()
@property (nonatomic ,weak) UILabel *timeLabel;
@property (nonatomic ,weak) UIButton *btnMessage;
@property (nonatomic ,weak) UIImageView *iconView;

@end

@implementation WHSMessageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //时间
        UILabel *timeLabel = [[UILabel alloc ] init];
        //设置文字大小
        timeLabel.font = [UIFont systemFontOfSize:12];
        //设置文字居中
        timeLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:timeLabel];
         self.timeLabel = timeLabel;
        
        //消息
        UIButton *btnMessage = [[UIButton alloc ]init];
        btnMessage.titleLabel.font = textFont;
        [btnMessage setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        //设置文字可以换行
        btnMessage.titleLabel.numberOfLines = 0;
        //设置按钮的内边距
        btnMessage.contentEdgeInsets = UIEdgeInsetsMake(15, 20, 15, 20);
        [self.contentView addSubview:btnMessage];
        self.btnMessage =btnMessage;
        
        //头像
        
        UIImageView *iconView = [[UIImageView alloc]init];
        [self.contentView addSubview:iconView];
        self.iconView = iconView;
    }
        //设置单元格背景色为clearColor
        self.backgroundColor = [UIColor clearColor];
        return self;
        
  
}

+ (instancetype)messageCellWithTableViwe:(UITableView *)tableView{
    static NSString *ID = @"message_cell";
    WHSMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        cell = [[WHSMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}
- (void)setMessageFrame:(WHSMessageFrame *)messageFrame{
    _messageFrame = messageFrame;
    WHSMessage *messageModel =messageFrame.message;
    //时间
    self.timeLabel.text = messageModel.time;
    self.timeLabel.frame = messageFrame.timeFrame;
    self.timeLabel.hidden = messageModel.hideTime;
    
    //头像 应根据消息类型
    NSString *iconImg = messageModel.type == WHSMessageTypeMe ? @"me" : @"other";
    self.iconView.image = [UIImage imageNamed:iconImg];
    self.iconView.frame = messageFrame.iconFrame;
    
    //设置消息正文
    [self.btnMessage setTitle:messageModel.text forState:UIControlStateNormal];
    self.btnMessage.frame = messageFrame.messageFrame;
    
    //正文的n背景图
    NSString *imgNor, *imgHighlighted;
    if (messageModel.type == WHSMessageTypeMe) {
        imgNor = @"chat_send_nor";
        imgHighlighted = @"chat_send_press_pic";
        //设置消息的正文的文字颜色为白色
          [self.btnMessage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else{
        imgNor = @"chat_receive_nor";
             imgHighlighted = @"chat_recive_press_pic";
             //设置消息的正文的文字颜色为黑色
               [self.btnMessage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    //加载图片
    UIImage *imageNormal = [UIImage imageNamed:imgNor];
    UIImage *imageHighlighted = [UIImage imageNamed:imgHighlighted];
    
    //用平铺的方式拉伸图片
    imageNormal =[imageNormal stretchableImageWithLeftCapWidth:imageNormal.size.width*0.5 topCapHeight:imageNormal.size.height*0.5];
    imageHighlighted =[imageHighlighted stretchableImageWithLeftCapWidth:imageHighlighted.size.width*0.5 topCapHeight:imageHighlighted.size.height * 0.5];
    
    //设置n背景图
    [self.btnMessage setBackgroundImage:imageNormal forState:UIControlStateNormal];
    [self.btnMessage setBackgroundImage:imageHighlighted forState:UIControlStateHighlighted];
  
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
