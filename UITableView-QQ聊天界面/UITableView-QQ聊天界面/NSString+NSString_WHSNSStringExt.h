//
//  NSString+NSString_WHSNSStringExt.h
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/3/3.
//  Copyright © 2020 魏华生. All rights reserved.
//


#import <UIKit/UIKit.h>


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (NSString_WHSNSStringExt)

//对象方法
- (CGSize)sizeOfTextWithMaxSize:(CGSize)maxsize font:(UIFont *)font;

//类方法
+ (CGSize)sizeWithText:(NSString *)text maxsize:(CGSize)maxSize font:(UIFont *)font;

@end

NS_ASSUME_NONNULL_END
