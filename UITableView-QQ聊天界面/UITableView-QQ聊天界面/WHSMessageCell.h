//
//  WHSMessageCell.h
//  UITableView-QQ聊天界面
//
//  Created by 魏华生 on 2020/2/26.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WHSMessageFrame.h"

NS_ASSUME_NONNULL_BEGIN
@class WHSMessageFrame;
@interface WHSMessageCell : UITableViewCell
@property (nonatomic ,strong) WHSMessageFrame *messageFrame;
+ (instancetype)messageCellWithTableViwe:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
