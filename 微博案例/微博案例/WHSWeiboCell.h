//
//  WHSWeiboCell.h
//  微博案例
//
//  Created by 魏华生 on 2020/2/23.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WHSWeibo.h"
#import "WHSWeiboFrame.h"

/**
 不能用xib创建cell是因为每行所展示的数据不一样，有的有图片，有的没有，这就需要手动创建cell
 */

NS_ASSUME_NONNULL_BEGIN

@class WHSWeiboFrame;
@interface WHSWeiboCell : UITableViewCell

@property (nonatomic ,strong) WHSWeiboFrame *weiboFrame;

+ (instancetype)weiboCellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
