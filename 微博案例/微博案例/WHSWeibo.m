//
//  WHSWeibo.m
//  微博案例
//
//  Created by 魏华生 on 2020/2/23.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSWeibo.h"

@implementation WHSWeibo

- (instancetype)initWithDict:(NSDictionary *)dict{
    if (self=[super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)weiboWithDict:(NSDictionary *)dict{
    return [[self alloc]initWithDict:dict];
}
@end
