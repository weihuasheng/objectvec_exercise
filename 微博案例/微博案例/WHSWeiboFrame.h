//
//  WHSWeiboFrame.h
//  微博案例
//
//  Created by 魏华生 on 2020/2/24.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
#import "WHSWeibo.h"

#define nameFont [UIFont systemFontOfSize:12]
#define textFont [UIFont systemFontOfSize:14]

NS_ASSUME_NONNULL_BEGIN
@class WHSWeibo;
@interface WHSWeiboFrame : NSObject
@property (nonatomic ,strong)WHSWeibo *weibo;
// 用来保存头像的frame
@property (nonatomic, assign, readonly) CGRect iconFrame;

// 昵称的frame
@property (nonatomic, assign, readonly) CGRect nameFrame;


// vip的frame
@property (nonatomic, assign, readonly) CGRect vipFrame;

// 正文的frame
@property (nonatomic, assign, readonly) CGRect textFrame;

//配图的frame
@property (nonatomic, assign, readonly) CGRect picFrame;

// 行高
@property (nonatomic, assign, readonly) CGFloat rowHeight;

@end

NS_ASSUME_NONNULL_END
