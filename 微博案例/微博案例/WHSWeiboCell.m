//
//  WHSWeiboCell.m
//  微博案例
//
//  Created by 魏华生 on 2020/2/23.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSWeiboCell.h"
#define nameFont [UIFont systemFontOfSize:12]
#define textFont [UIFont systemFontOfSize:14]

@interface WHSWeiboCell()
@property (nonatomic ,weak) UIImageView *iconView;
@property (nonatomic ,weak) UILabel *lblName;
@property (nonatomic ,weak) UIImageView *imageVip;
@property (nonatomic ,weak) UILabel *lblText;
@property (nonatomic ,weak) UIImageView *imgPicture;


@end

@implementation WHSWeiboCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (instancetype)weiboCellWithTableView:(UITableView *)tableView{
    static NSString *ID = @"weibo_cell";
    WHSWeiboCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell==nil){
        cell = [[WHSWeiboCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

//重写initWithStyle方法，创建所需要的控件，控件的位置以及内容不需要关心
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UIImageView *iconView = [[UIImageView alloc] init];
        [self.contentView addSubview:iconView];//把子控件加到uitableview，要加入到contentView里面
        self.iconView = iconView;//不然不展示
        
        UILabel *lblName = [[UILabel alloc]init];
        [self.contentView addSubview:lblName];
        self.lblName = lblName;
        self.lblName.font=nameFont;
        
        UIImageView *imageVip = [[UIImageView alloc] init];
        imageVip.image = [UIImage imageNamed:@"vip"];
        [self.contentView addSubview:imageVip];
        self.imageVip = imageVip;
        
        UILabel *lbltext = [[UILabel alloc] init];
        lbltext.numberOfLines = 0;//自动换行
        lbltext.font = textFont;
        [self.contentView addSubview:lbltext];
        self.lblText = lbltext;
        
        
        UIImageView *imgPicture = [[UIImageView alloc] init];
        [self.contentView addSubview:imgPicture];
        self.imgPicture = imgPicture;
        
    }
    return self;
}

//重写weibo的set方法
- (void)setWeiboFrame:(WHSWeiboFrame *)weiboFrame{
    _weiboFrame = weiboFrame;//一定要
    
    //由于有五个子控件，都写在这个方法里不好看，故抽调为两个方法
    
    //设置当前单元格中的子控件的数据
    [self settingData];
    
    //设置a当前单元格的子控件的frame
    [self settingFrame];
    
    
}

//设置数据
- (void)settingData{
    WHSWeibo *model = self.weiboFrame.weibo;
    //图头像
    self.iconView.image = [UIImage imageNamed:model.icon];
    //昵称
    self.lblName.text = model.name;
    //vip
    if([model isVip]){
        self.imageVip.hidden = NO;
    }else{
        self.imageVip.hidden = YES;
    }
    //配图
    if (model.picture==nil) {
        self.imgPicture.hidden = YES; //如果不隐藏，上一个重用的cell 假如有图片就会显示
    }else{
        self.imgPicture.image= [UIImage imageNamed:model.picture];//读取空的图片名称会异常
        self.imgPicture.hidden = NO;
    }
    //正文
    self.lblText.text = model.text;
    
    
    
    
}

//设置frame
- (void)settingFrame{
    
//    CGFloat margin = 10;
//    //头像
//    CGFloat iconW = 35;
//    CGFloat iconH = 35;
//    CGFloat iconX = margin;
//    CGFloat iconY = 0;
//    self.iconView.frame = CGRectMake(iconX, iconY, iconW, iconH);
//
//    //昵称
//    CGFloat nameX = CGRectGetMaxX(self.iconView.frame)+margin;
//    CGSize nameSize = [self sizeWithText:self.lblName.text andMaxsize:CGSizeMake(MAXFLOAT, MAXFLOAT) andFont:nameFont];
//    CGFloat nameW = nameSize.width;
//    CGFloat nameH = nameSize.height;
//    CGFloat nameY = iconY+(iconH - nameH) *0.5;
//    self.lblName.frame =CGRectMake(nameX, nameY, nameW, nameH);
//
//    //vip
//    CGFloat vipX = CGRectGetMaxX(self.lblName.frame) +margin;
//    CGFloat vipY = nameY;
//    CGFloat vipW = 10;
//    CGFloat vipH = 10;
//    self.imageVip.frame = CGRectMake(vipX, vipY, vipW, vipH);
//
//    //正文
//    CGFloat textX = iconX;
//    CGFloat textY = CGRectGetMaxY(self.iconView.frame)+margin;
//    CGSize textSize = [self sizeWithText:self.lblText.text andMaxsize:CGSizeMake(350, MAXFLOAT) andFont:textFont];
//    CGFloat textW = textSize.width;
//    CGFloat textH = textSize.height;
//    self.lblText.frame = CGRectMake(textX, textY, textW, textH);
//
//    //图片
//    CGFloat picW = 100;
//    CGFloat picH = 100;
//    CGFloat picX = iconX;
//    CGFloat picY = CGRectGetMaxY(self.lblText.frame)+margin;
//    self.imgPicture.frame = CGRectMake(picX, picY, picW, picH);
//
//    //行高
//    CGFloat rowHeight = 0;
//    if(self.weibo.picture){
//        rowHeight = CGRectGetMaxY(self.imgPicture.frame)+margin;
//    }else{
//        rowHeight = CGRectGetMaxY(self.lblText.frame)+margin;
//    }
//
//    //设置了行高，在WHSTableViewController里使用就要用代理，但是不能正常设置代理，因为设置行高的代理方法优先于设置控件的方法
//}
//
//
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    self.iconView.frame = self.weiboFrame.iconFrame;
    self.lblName.frame = self.weiboFrame.nameFrame;
    self.imageVip.frame = self.weiboFrame.vipFrame;
    self.lblText.frame = self.weiboFrame.textFrame;
    self.imageView.frame = self.weiboFrame.picFrame;
}

//根据给定的字符串，最大值的size，给定的的字符串，给定的字体，来计算文字应该占用的大小
//-(CGSize) sizeWithText:(NSString *)text andMaxsize:(CGSize)maxsize  andFont:(UIFont *)font{
//    NSDictionary *attr = @{NSFontAttributeName :font};
//    return [text boundingRectWithSize:maxsize options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
//
//}
@end
