//
//  WHSWeibo.h
//  微博案例
//
//  Created by 魏华生 on 2020/2/23.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSWeibo : NSObject
@property (nonatomic ,copy) NSString *text;
@property (nonatomic ,copy) NSString *icon;
@property (nonatomic ,copy) NSString *name;
@property (nonatomic ,assign ,getter=isVip) BOOL vip;
@property (nonatomic ,copy) NSString *picture;

- (instancetype) initWithDict: (NSDictionary*) dict;
+ (instancetype) weiboWithDict: (NSDictionary*) dict;
@end

NS_ASSUME_NONNULL_END
