//
//  WHSTableViewController.m
//  微博案例
//
//  Created by 魏华生 on 2020/2/23.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSTableViewController.h"
#import "WHSWeibo.h"
#import "WHSWeiboCell.h"
#import "WHSWeiboFrame.h"

@interface WHSTableViewController ()
//现在要求weiboFrames集合中保存的很多WHSWeiboFrame模型，不再是WHSWeibo模型了。
@property (nonatomic ,strong) NSArray *weiboFrames;

@end

@implementation WHSTableViewController

- (NSArray *)weiboFrames{
    if (_weiboFrames==nil) {
        NSString *path = [[NSBundle mainBundle]pathForResource:@"weibos.plist" ofType:nil];
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        NSMutableArray *arrayM = [NSMutableArray array];
        for (NSDictionary *dict in array) {
            WHSWeibo *model = [WHSWeibo weiboWithDict:dict];
            WHSWeiboFrame *modelFrame =[[WHSWeiboFrame alloc]init];
            modelFrame.weibo=model;
            [arrayM addObject:modelFrame];
        }
        _weiboFrames = arrayM;
    }
    return _weiboFrames;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[WHSWeiboCell class] forCellReuseIdentifier:@"weibo_cell"];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.weiboFrames.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WHSWeiboFrame *model = self.weiboFrames[indexPath.row];
    WHSWeiboCell *cell = [WHSWeiboCell weiboCellWithTableView:tableView];
    
    //设置单元格数据
    cell.weiboFrame = model;
    
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    WHSWeiboFrame *weiboFrame = self.weiboFrames[indexPath.row];
    return weiboFrame.rowHeight;
}//比cellForRow方法先调用


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
