//
//  WHSWeiboFrame.m
//  微博案例
//
//  Created by 魏华生 on 2020/2/24.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSWeiboFrame.h"

@implementation WHSWeiboFrame
- (void)setWeibo:(WHSWeibo *)weibo{
    _weibo = weibo;
    CGFloat margin = 10;
    //头像
    CGFloat iconW = 35;
    CGFloat iconH = 35;
    CGFloat iconX = margin;
    CGFloat iconY = 0;
    _iconFrame = CGRectMake(iconX, iconY, iconW, iconH);
    
    //昵称
    CGFloat nameX = CGRectGetMaxX(_iconFrame)+margin;
    CGSize nameSize = [self sizeWithText:weibo.name andMaxsize:CGSizeMake(MAXFLOAT, MAXFLOAT) andFont:nameFont];
    CGFloat nameW = nameSize.width;
    CGFloat nameH = nameSize.height;
    CGFloat nameY = iconY+(iconH - nameH) *0.5;
    _nameFrame =CGRectMake(nameX, nameY, nameW, nameH);
    
    //vip
    CGFloat vipX = CGRectGetMaxX(_nameFrame) +margin;
    CGFloat vipY = nameY;
    CGFloat vipW = 10;
    CGFloat vipH = 10;
    _vipFrame = CGRectMake(vipX, vipY, vipW, vipH);
    
    //正文
    CGFloat textX = iconX;
    CGFloat textY = CGRectGetMaxY(_iconFrame)+margin;
    CGSize textSize = [self sizeWithText:weibo.text andMaxsize:CGSizeMake(350, MAXFLOAT) andFont:textFont];
    CGFloat textW = textSize.width;
    CGFloat textH = textSize.height;
    _textFrame = CGRectMake(textX, textY, textW, textH);
    
    //图片
    CGFloat picW = 100;
    CGFloat picH = 100;
    CGFloat picX = iconX;
    CGFloat picY = CGRectGetMaxY(_textFrame)+margin;
    _picFrame= CGRectMake(picX, picY, picW, picH);
    
    //行高
    CGFloat rowHeight = 0;
    if(self.weibo.picture){
        rowHeight = CGRectGetMaxY(_picFrame)+margin;
    }else{
        rowHeight = CGRectGetMaxY(_textFrame)+margin;
    }
    _rowHeight = rowHeight;
}
-(CGSize) sizeWithText:(NSString *)text andMaxsize:(CGSize)maxsize  andFont:(UIFont *)font{
    NSDictionary *attr = @{NSFontAttributeName :font};
    return [text boundingRectWithSize:maxsize options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
    
}
@end
