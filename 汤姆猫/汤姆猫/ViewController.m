//
//  ViewController.m
//  汤姆猫
//
//  Created by 魏华生 on 2020/1/28.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)drink;
- (IBAction)scrate;
- (IBAction)eat;
- (IBAction)fart;
- (IBAction)cymbal;
- (IBAction)pie;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)drink {
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<81;i++){
    NSString *imgName=[NSString stringWithFormat:@"drink_%02d",i];
        UIImage *imgDrink=[UIImage imageNamed:imgName];
        [arrayM addObject:imgDrink];
    }
    self.imgView.animationImages=arrayM;
    self.imgView.animationDuration=arrayM.count*0.1;
    self.imgView.animationRepeatCount=1;
    [self.imgView startAnimating];
}

- (IBAction)scrate {
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<56;i++){
    NSString *imgName=[NSString stringWithFormat:@"scratch_%02d",i];
        UIImage *imgDrink=[UIImage imageNamed:imgName];
        [arrayM addObject:imgDrink];
    }
    self.imgView.animationImages=arrayM;
    self.imgView.animationDuration=arrayM.count*0.1;
    self.imgView.animationRepeatCount=1;
    [self.imgView startAnimating];

}

- (IBAction)eat {
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<40;i++){
    NSString *imgName=[NSString stringWithFormat:@"eat_%02d",i];
        UIImage *imgDrink=[UIImage imageNamed:imgName];
        [arrayM addObject:imgDrink];
    }
    self.imgView.animationImages=arrayM;
    self.imgView.animationDuration=arrayM.count*0.1;
    self.imgView.animationRepeatCount=1;
    [self.imgView startAnimating];

}

- (IBAction)fart {
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<28;i++){
    NSString *imgName=[NSString stringWithFormat:@"fart_%02d",i];
        UIImage *imgDrink=[UIImage imageNamed:imgName];
        [arrayM addObject:imgDrink];
    }
    self.imgView.animationImages=arrayM;
    self.imgView.animationDuration=arrayM.count*0.1;
    self.imgView.animationRepeatCount=1;
    [self.imgView startAnimating];

}

- (IBAction)cymbal {
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<13;i++){
    NSString *imgName=[NSString stringWithFormat:@"cymbal_%02d",i];
        UIImage *imgDrink=[UIImage imageNamed:imgName];
        [arrayM addObject:imgDrink];
    }
    self.imgView.animationImages=arrayM;
    self.imgView.animationDuration=arrayM.count*0.1;
    self.imgView.animationRepeatCount=1;
    [self.imgView startAnimating];

}

- (IBAction)pie {
    NSMutableArray *arrayM=[NSMutableArray array];
    for(int i=0;i<24;i++){
//    NSString *imgName=[NSString stringWithFormat:@"pie_%02d",i];
//        UIImage *imgDrink=[UIImage imageNamed:imgName];
//        [arrayM addObject:imgDrink];
        NSString *imgName=[NSString stringWithFormat:@"pie_%02d.jpg",i];
               NSString *path=[[NSBundle mainBundle] pathForResource:imgName ofType:nil];
               UIImage *img=[UIImage imageWithContentsOfFile:path];
               [arrayM addObject:img];
    }
    self.imgView.animationImages=arrayM;
    self.imgView.animationDuration=arrayM.count*0.1;
    self.imgView.animationRepeatCount=1;
    [self.imgView startAnimating];

}

-(void)setAnimation{
    
}
@end
