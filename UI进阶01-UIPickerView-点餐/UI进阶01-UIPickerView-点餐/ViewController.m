//
//  ViewController.m
//  UI进阶01-UIPickerView-点餐
//
//  Created by weihuasheng on 2020/5/15.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic, strong)NSArray *foods;
@property (weak, nonatomic) IBOutlet UILabel *fruitLbl;
@property (weak, nonatomic) IBOutlet UILabel *mainFoodLbl;
@property (weak, nonatomic) IBOutlet UILabel *drinkLbl;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@end

@implementation ViewController

//随机点餐
- (IBAction)btnOrder:(id)sender {
    for (int i=0; i<self.foods.count; i++) {
        NSUInteger count = [self.foods[i] count];
        u_int32_t randomFood = arc4random_uniform((int)count);
        //获取第i组当前选中的行
        NSUInteger selected = [self.pickerView selectedRowInComponent:i];
        
        while (selected == randomFood) {
            randomFood = arc4random_uniform((int)count);
        }
        [self.pickerView selectRow:randomFood inComponent:i animated:YES];
        [self pickerView:self.pickerView didSelectRow:randomFood inComponent:i];
    }

}

- (NSArray *)foods{
    if (_foods == nil) {
        _foods = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle ]pathForResource:@"01foods.plist" ofType:nil]];
    }
    return _foods;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    for (int i=0; i<self.foods.count; i++) {
         [self pickerView:self.pickerView didSelectRow:0 inComponent:i];
    }
   
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return self.foods.count;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSArray *arra = self.foods[component];
    return arra.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *arra = self.foods[component];
    return arra[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSArray *arra = self.foods[component];
    switch (component) {
        case 0:
            self.fruitLbl.text = arra[row];
            break;
        case 1:
            self.mainFoodLbl.text = arra[row];
            break;
        case 2:
            self.drinkLbl.text = arra[row];
            break;
            
        default:
            break;
    }
}

@end
