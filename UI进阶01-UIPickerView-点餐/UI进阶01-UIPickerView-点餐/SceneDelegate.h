//
//  SceneDelegate.h
//  UI进阶01-UIPickerView-点餐
//
//  Created by weihuasheng on 2020/5/15.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

