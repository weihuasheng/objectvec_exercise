//
//  ViewController.m
//  autolayout（代码实现）
//
//  Created by weihuasheng on 2020/5/8.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //1.创建蓝色view（不用创建frame）
    UIView *blueView = [[UIView alloc]init];
    blueView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:blueView];
    //2.创建红色view
    UIView *redView = [[UIView alloc]init];
    redView.backgroundColor = [UIColor redColor];
    [self.view addSubview:redView];
    
    //3.禁止autoresizing
    blueView.translatesAutoresizingMaskIntoConstraints = NO;
    redView.translatesAutoresizingMaskIntoConstraints = NO;
    //4.创建约束
        //4.1创建蓝色view的约束
            //距离左边30
    NSLayoutConstraint *leftConsrtaint = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:blueView.superview attribute:NSLayoutAttributeLeft multiplier:1.0 constant:30];
    
    [self.view addConstraint:leftConsrtaint];
            //距离上边30
      NSLayoutConstraint *upConsrtaint = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:blueView.superview attribute:NSLayoutAttributeTop multiplier:1.0 constant:30];
    
       [self.view addConstraint:upConsrtaint];
            //距离右边30
      NSLayoutConstraint *rightConsrtaint = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:blueView.superview attribute:NSLayoutAttributeRight multiplier:1.0 constant:-30];
    
       [self.view addConstraint:rightConsrtaint];
            //高度50
      NSLayoutConstraint *heightConsrtaint = [NSLayoutConstraint constraintWithItem:blueView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0 constant:50];
    
    [blueView addConstraint:heightConsrtaint];
    
    
    //4.2创建红色view的约束
        //红色view的高度与蓝色的相同
    NSLayoutConstraint *redHeight = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:blueView attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    
    [self.view addConstraint:redHeight];
        //红色view等于 蓝色view的宽度的一半
    NSLayoutConstraint *redWidth = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:blueView attribute:NSLayoutAttributeWidth multiplier:0.5 constant:0];
     
     [self.view addConstraint:redWidth];
        //红色view与蓝色view的间距为10
    NSLayoutConstraint *redMargin = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:blueView attribute:NSLayoutAttributeBottom multiplier:1 constant:10];
     
     [self.view addConstraint:redMargin];
        //红色view与蓝色view右对齐
    NSLayoutConstraint *redRight = [NSLayoutConstraint constraintWithItem:redView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:blueView attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
     
     [self.view addConstraint:redRight];
    
}


@end
