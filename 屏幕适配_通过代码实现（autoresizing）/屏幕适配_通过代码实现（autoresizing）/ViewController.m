//
//  ViewController.m
//  屏幕适配_通过代码实现（autoresizing）
//
//  Created by weihuasheng on 2020/4/17.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, weak) UIView *blueView;
@property (nonatomic, weak) UIView *redView;
- (IBAction)btnClick:(id)sender;

@end

@implementation ViewController

/**
 要求：
 当蓝色Vie发生变化时（宽和高发生变化），红色View的宽随着蓝色view的宽度变化而变化（红色view的高度始终保持不变），
 并且永远紧贴着蓝色view的底部显示
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    //创建一个蓝色view
    UIView *blueView = [[UIView alloc ]init];
    //设置背景色
    [blueView setBackgroundColor:[UIColor blueColor]];
    //设置frame
    [blueView setFrame:CGRectMake(20, 20, 150, 150)];
    self.blueView = blueView;
    [self.view addSubview:blueView];
    
    //创建红色view
    UIView *redView = [[UIView alloc]init];
    [redView setBackgroundColor:[UIColor redColor]];
    redView.frame = CGRectMake(0,120 , 65, 30);
    self.redView = redView;
    [blueView addSubview:redView];
    
    //设置autoresizing
    //1.设置红色view距离蓝色view的底部的距离保持不变
    redView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
}


- (IBAction)btnClick:(id)sender {
    CGRect originFrame = self.blueView.frame;
    originFrame.size.height +=10;
    originFrame.size.width +=10;
    self.blueView.frame = originFrame;
    
}
@end
