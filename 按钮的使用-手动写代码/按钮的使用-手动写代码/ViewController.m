//
//  ViewController.m
//  按钮的使用-手动写代码
//
//  Created by 魏华生 on 2020/1/23.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


// 当要显示一个界面的时候, 首先创建这个界面对应的控制器
// 控制器创建好以后, 接着创建控制器所管理的那个view， 当这个view加载完毕以后就开始执行下面的方法了。
// 所以只要viewDidLoad方法被执行了, 就表示控制器所管理的view创建好了

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIButton *myButton=[UIButton buttonWithType:UIButtonTypeCustom];

      
    //默认状态
    UIImage *imageNormal=[UIImage imageNamed:@"btn_01"];
    [myButton setTitle:@"点我啊" forState:UIControlStateNormal];
    [myButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [myButton setBackgroundImage:imageNormal forState:UIControlStateNormal];
    
    //点击的状态
    UIImage *imageHlighted=[UIImage imageNamed:@"btn_02"];
    [myButton setTitle:@"摸我干啥" forState:UIControlStateHighlighted];
    [myButton setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [myButton setBackgroundImage:imageHlighted forState:UIControlStateHighlighted];
    

    
    //设置按钮的大小，否则不能正常显示
    myButton.frame=CGRectMake(300, 500, 100, 100);

    //将按钮添加到frame上
    [self.view addSubview:myButton];
    
    //为按钮添加事件
    [myButton addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)buttonClicked{
    NSLog(@"i am iron-man");
}

@end
