//
//  Dog.h
//  protocol_test
//
//  Created by student_weihuasheng on 2019/12/10.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProtocol.h"

NS_ASSUME_NONNULL_BEGIN
/*
 Dog类实现了协议的方法，那么这个类就要实现协议中的方法，当然不实现也能正常通过编译，只会警告
 */

@interface Dog : NSObject<MyProtocol>

@end

NS_ASSUME_NONNULL_END
