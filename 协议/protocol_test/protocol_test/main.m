//
//  main.m
//  protocol_test
//
//  Created by student_weihuasheng on 2019/12/10.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dog.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Dog *mydog=[[Dog alloc]init];
        [mydog run];
        [mydog sleep];
    }
    return 0;
}
