//
//  MyProtocol.h
//  protocol_test
//
//  Created by student_weihuasheng on 2019/12/10.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/*
    这个类声明一个协议
 */
@protocol MyProtocol <NSObject>
-(void)run;
-(void)sleep;

@end

NS_ASSUME_NONNULL_END
