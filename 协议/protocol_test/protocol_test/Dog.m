//
//  Dog.m
//  protocol_test
//
//  Created by student_weihuasheng on 2019/12/10.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import "Dog.h"
/*
 实现协议的方法
 */
@implementation Dog
-(void)run{
    NSLog(@"我快速地跑。。。。");
    
}

-(void)sleep{
    NSLog(@"我要睡了。。。。。");
}
@end
