//
//  ViewController.m
//  autolayout（动画）
//
//  Created by weihuasheng on 2020/5/8.
//  Copyright © 2020 weihuasheng. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)btn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *redView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)btn:(id)sender {
    self.top.constant += 100;
    [UIView animateWithDuration:1.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}
@end
