//
//  ViewController.m
//  应用管理器
//
//  Created by 魏华生 on 2020/1/30.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSApp.h"
#import "WHSAppView.h"

@interface ViewController ()
// 用来保存所有应用的数据
@property(nonatomic,strong) NSArray *pic;
@end

@implementation ViewController

//懒加载-重写get方法
- (NSArray *)pic{
    if(_pic==nil){
        // 1. 获取app.plist文件在手机上的路径
        NSString *path=[[NSBundle mainBundle] pathForResource:@"app.plist" ofType:nil];
           // 2. 根据路径加载数据
        NSArray *arrayDict=[NSArray arrayWithContentsOfFile:path];
        // 3. 创建一个可变数据用来保存一个一个的模型对象
        NSMutableArray *arrayModel=[NSMutableArray array];
        // 4. 循环字典数组, 把每个字典对象转换成一个模型对象
        for(NSDictionary *dict in arrayDict){
            // 创建一个模型
            WHSApp *model=[WHSApp appWithDict:dict];
              // 把模型加到arrayModels中
            [arrayModel addObject:model];
        }
        _pic=arrayModel;
    }
    return _pic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    int countX=3;//每行显示3个应用
    CGFloat viewWidth=self.view.frame.size.width;
    CGFloat picWidth=75;//每个view的长度
    CGFloat picHeight=90;//每个view的高度；
    CGFloat marginX=(viewWidth-picWidth*countX)/4.0;//view距离左边的边界
    CGFloat marginTop=45;//第一行view距离上边界的距离
    CGFloat picX;//每个view的x坐标
    CGFloat picY;//每个view的y坐标
    for (int i=0; i<self.pic.count; i++) {
//          NSBundle *rootBundle=[NSBundle mainBundle];
//          UIView *appView=[[rootBundle loadNibNamed:@"WHSAppView" owner:nil options:nil] firstObject];
        WHSAppView *appView=[WHSAppView appView];
        WHSApp *appModel=self.pic[i];
          picX=marginX+(picWidth+marginX)*(i%countX);
          picY=marginTop+(picHeight+marginX)*(i/countX);
          appView.frame=CGRectMake(picX, picY, picWidth, picHeight);
          [self.view addSubview:appView];
        // 设置数据
           // 把模型数据设置给“自定义view”的model属性
           // 然后重写model属性的set方法, 在set方法中解析模型对象中的属性, 并把属性值设置给自定义view的各个子控件
        appView.model=appModel;
        
    }
    
   
        
    
    
}



@end
