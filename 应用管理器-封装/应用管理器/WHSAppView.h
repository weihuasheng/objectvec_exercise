//
//  WHSAppView.h
//  应用管理器
//
//  Created by 魏华生 on 2020/1/31.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WHSApp;
NS_ASSUME_NONNULL_BEGIN

@interface WHSAppView : UIView
@property(nonatomic,strong) WHSApp *model;

+(instancetype)appView;
@end

NS_ASSUME_NONNULL_END
