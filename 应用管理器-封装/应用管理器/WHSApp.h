//
//  WHSApp.h
//  应用管理器
//
//  Created by 魏华生 on 2020/1/30.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSApp : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy)NSString *icon;

-(instancetype)initWithDict:(NSDictionary *)dict;
+(instancetype)appWithDict:(NSDictionary *)dict;
//相当于+(WHSApp *)appWithDict:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
