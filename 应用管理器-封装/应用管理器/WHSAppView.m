//
//  WHSAppView.m
//  应用管理器
//
//  Created by 魏华生 on 2020/1/31.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSAppView.h"
#import "WHSApp.h"
@interface WHSAppView()
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *appLabel;
- (IBAction)btnDownload:(UIButton *)sender;


@end


@implementation WHSAppView
- (void)setModel:(WHSApp *)model{
    _model=model;
    self.iconView.image=[UIImage imageNamed:model.icon];
    self.appLabel.text=model.name;
}
+(instancetype)appView{
    NSBundle *rootBundle=[NSBundle mainBundle];
    return [[rootBundle loadNibNamed:@"WHSAppView" owner:nil options:nil] firstObject];
}

- (IBAction)btnDownload:(UIButton *)sender {
    
}
@end
