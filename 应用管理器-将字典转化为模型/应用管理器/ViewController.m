//
//  ViewController.m
//  应用管理器
//
//  Created by 魏华生 on 2020/1/30.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSApp.h"

@interface ViewController ()

@property(nonatomic,strong) NSArray *pic;
@end

@implementation ViewController

//懒加载-重写get方法
- (NSArray *)pic{
    if(_pic==nil){
        // 1. 获取app.plist文件在手机上的路径
        NSString *path=[[NSBundle mainBundle] pathForResource:@"app.plist" ofType:nil];
           // 2. 根据路径加载数据
        NSArray *arrayDict=[NSArray arrayWithContentsOfFile:path];
        // 3. 创建一个可变数据用来保存一个一个的模型对象
        NSMutableArray *arrayModel=[NSMutableArray array];
        // 4. 循环字典数组, 把每个字典对象转换成一个模型对象
        for(NSDictionary *dict in arrayDict){
            // 创建一个模型
            WHSApp *model=[WHSApp appWithDict:dict];
              // 把模型加到arrayModels中
            [arrayModel addObject:model];
        }
        _pic=arrayModel;
    }
    return _pic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    int countX=3;//每行显示3个应用
    CGFloat viewWidth=self.view.frame.size.width;
    CGFloat picWidth=75;//每个view的长度
    CGFloat picHeight=90;//每个view的高度；
    CGFloat marginX=(viewWidth-picWidth*countX)/4.0;//view距离左边的边界
    CGFloat marginTop=45;//第一行view距离上边界的距离
    CGFloat picX;//每个view的x坐标
    CGFloat picY;//每个view的y坐标
    for (int i=0; i<self.pic.count; i++) {
          UIView *appView=[[UIView alloc] init];
          picX=marginX+(picWidth+marginX)*(i%countX);
          picY=marginTop+(picHeight+marginX)*(i/countX);
          appView.frame=CGRectMake(picX, picY, picWidth, picHeight);
          [self.view addSubview:appView];
        
        //添加子控件
        //添加图片框
        UIImageView *imageIcon=[[UIImageView alloc] init];
        CGFloat iconW=45;
        CGFloat iconH=45;
        CGFloat iconX=(appView.frame.size.width-iconW)/2;
        CGFloat iconY=0;
        imageIcon.frame=CGRectMake(iconX, iconY, iconW, iconH);
        [appView addSubview:imageIcon];
        
        //添加图片框数据
        WHSApp *apps=self.pic[i];
        imageIcon.image=[UIImage imageNamed:apps.icon];
        
        //添加标题
        UILabel *label=[[UILabel alloc] init];
        CGFloat labelW=picWidth;
        CGFloat labelH=20;
        CGFloat labelX=0;
        CGFloat labelY=iconH;
        label.frame=CGRectMake(labelX, labelY, labelW, labelH);
        [appView addSubview:label];
        
        //添加标题数据
        label.text=apps.name;
        label.font=[UIFont systemFontOfSize:12];
        label.textAlignment=NSTextAlignmentCenter ;
        
        //添加按钮
        UIButton *button=[[UIButton alloc]init];
        CGFloat btnW=iconW;
        CGFloat btnH=picHeight-iconH-labelH;
        CGFloat btnX=iconX;
        CGFloat btnY=iconH+labelH;
        button.frame=CGRectMake(btnX, btnY, btnW, btnH);
        [appView addSubview:button];
        
        //为按钮添加数据
        [button setTitle:@"下载" forState:UIControlStateNormal];
        [button setTitle:@"已安装" forState:UIControlStateDisabled];
        [button setBackgroundImage:[UIImage imageNamed:@"buttongreen"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"buttongreen_highlighted"] forState:UIControlStateDisabled];
        button.titleLabel.font=[UIFont systemFontOfSize:14];
        
        
    }
    
   
        
    
    
}



@end
