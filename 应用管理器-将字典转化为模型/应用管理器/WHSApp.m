//
//  WHSApp.m
//  应用管理器
//
//  Created by 魏华生 on 2020/1/30.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSApp.h"

//6. 字典转模型
//0> 字典转模型的含义: 把字典中的数据使用模型来保存。新建一个类, 根据字典中键值对的个数, 来编写这个类中的属性, 将来用这个类的对象的属性来保存字典中每个键对应的值。
//
//1> 为什么要把字典转成模型?
//* 字典缺陷:
//0> 写代码的时候字典的键没有智能提示, 但是模型的属性可以有智能提示
//1> "键"是字符串, 如果写错了, 编译器不报错(在编译的时候不报错), 运行时可能出错, 出错了很难找错。
//2> 使用"模型"可以更方便的使用面向对象的3大特(封装、继承、多态)性进行扩展。
//
//* 什么是模型? "模型"就是自定义的类, 通过为"类"增加各种属性来保存数据。
//
//* 字典转模型要修改哪里的代码?
//1> 创建一个模型类
//2> 在懒加载数据的时候, 把加载到的数据都放到模型对象中, 然后再把模型对象放到数组中。
//
//
//* 复习把app.plist转换成模型数组的过程(参考ppt"字典转模型的过程")
//
//
//
//* 把字典转模型的过程封装到"模型"内部
//* 原因: 将来的这个"模型"可能会在很多地方被用到(比如有很多个控制器都会使用这个模型), 那么每次用到模型的地方都需要写一次把字典中的数据赋给模型属性的代码, 此时如果把这些赋值语句封装到模型内部, 会大大简化了使用复杂度与代码量。
//* 思路:
//1> 在模型中接受一个NSDictionary的参数, 然后在模型内部把NSDictioanry中的数据赋值给模型的属性。
//2> 封装一个initWithDict方法和一个appWithDict方法
//
//
//* id与instancetype的介绍
//1. 使用id作为方法返回值的问题:
//1> 在接收方法的返回值的时候可以使用任何类型来接收, 编译都不报错, 但是运行时可能出错。
//
//2. instancetype需要注意的点
//1> instancetype在类型表示上, 与id意思一样, 都表示任何对象类型
//2> instancetype只能用作返回值类型, 不能向id一样声明变量、用作参数等
//3> 使用instancetype, 编译器会检测instancetype的真实类型, 如果类型不匹配, 编译时就报错了。(instancetype出现在哪个类型中就表示对应的类型)

@implementation WHSApp
-(instancetype)initWithDict:(NSDictionary *)dict{
    if(self=[super init]){
        self.name=dict[@"name"];
        self.icon=dict[@"icon"];
        
        
    }
    return self;
}

+(instancetype)appWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}
@end
