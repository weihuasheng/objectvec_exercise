//
//  WHSApp.m
//  UITableView_应用下载
//
//  Created by 魏华生 on 2020/3/27.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSApp.h"

@implementation WHSApp
- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self = [super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)appWithDict:(NSDictionary *)dict{
    return [[self alloc]initWithDict:dict];
}

@end
