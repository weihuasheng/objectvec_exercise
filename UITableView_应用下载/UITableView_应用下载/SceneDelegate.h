//
//  SceneDelegate.h
//  UITableView_应用下载
//
//  Created by 魏华生 on 2020/3/27.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

