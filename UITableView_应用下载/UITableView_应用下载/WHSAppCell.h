//
//  WHSAppCell.h
//  UITableView_应用下载
//
//  Created by weihuasheng on 2020/4/10.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class WHSAppCell;
@protocol WHSAppCellDelegate <NSObject>

- (void)appCellDidClickDownloadButton:(WHSAppCell *)appCell;
@end


@class WHSApp;
@interface WHSAppCell : UITableViewCell
@property (nonatomic, strong)WHSApp *app;
@property (nonatomic, weak) id<WHSAppCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
