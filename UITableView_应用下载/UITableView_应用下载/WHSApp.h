//
//  WHSApp.h
//  UITableView_应用下载
//
//  Created by 魏华生 on 2020/3/27.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSApp : NSObject

//软件大小
@property(nonatomic, strong) NSString *size;
//下载量
@property(nonatomic, strong) NSString *download;
//软件名称
@property(nonatomic, strong) NSString *name;
//软件图片
@property(nonatomic, strong) NSString *icon;

//增加一个用来标记是否被“下载过”（点击过）的一个属性
@property(nonatomic,assign) BOOL isDownloaded;


- (instancetype)initWithDict: (NSDictionary *)dict;
+ (instancetype)appWithDict: (NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
