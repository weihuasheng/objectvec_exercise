//
//  WHSAppCell.m
//  UITableView_应用下载
//
//  Created by weihuasheng on 2020/4/10.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSAppCell.h"
#import "WHSApp.h"
@interface WHSAppCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblIntro;
@property (weak, nonatomic) IBOutlet UIButton *btnDownload;
- (IBAction)btnDownloadClick;


@end
@implementation WHSAppCell
/**
 要现在main.storyboard中点击app_cell，打开属性设置其class
 */

- (void)setApp:(WHSApp *)app{
    _app = app;
    
    //把app模型的数据设置给单元格的字控件
    self.imgViewIcon.image = [UIImage imageNamed:app.icon];
    self.lblName.text = app.name;
    self.lblIntro.text = [NSString stringWithFormat:@"大小：%@ |下载量： %@",app.size,app.download];
    
    //更新下载的按钮状态
    if (app.isDownloaded) {
        self.btnDownload.enabled = NO;
    }else{
        self.btnDownload.enabled = YES;
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnDownloadClick {
    NSLog(@"clicked");
    //1.禁用按钮的点击事件
    self.btnDownload.enabled = NO;
    //设置模型（标记一下已经被点击过过了）
    self.app.isDownloaded = YES;
    
    //2.弹出消息提示框label
    if ([self.delegate respondsToSelector:@selector(appCellDidClickDownloadButton:)]) {
        [self.delegate appCellDidClickDownloadButton:self];
    }
    
    
}
@end
