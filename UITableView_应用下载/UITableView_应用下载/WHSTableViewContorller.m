//
//  WHSTableViewContorller.m
//  UITableView_应用下载
//
//  Created by 魏华生 on 2020/3/28.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSTableViewContorller.h"
#import "WHSApp.h"
#import "WHSAppCell.h"

@interface WHSTableViewContorller () <WHSAppCellDelegate>

@property (nonatomic, strong) NSArray *apps;

@end

@implementation WHSTableViewContorller

#pragma marl - WHSAppCell的代理方法
- (void)appCellDidClickDownloadButton:(WHSAppCell *)appCell{
    //1.创建一个label
    UILabel *lblMsg = [[UILabel alloc] init];
    //1.1设置文字
    lblMsg.text = @"正在下载。。。";
    //1.2这是背景色
    lblMsg.backgroundColor = [UIColor blackColor];
    //1.3设置文字的颜色
    lblMsg.textColor = [UIColor redColor];
    //1.4设置文字的大小
    lblMsg.font = [UIFont systemFontOfSize:14];
    //1.5设置label里的文字居中
    lblMsg.textAlignment = NSTextAlignmentCenter;
    //1.6设置label的透明度
    lblMsg.alpha = 0.0;
    //1.7设置label的圆角显示
    lblMsg.layer.cornerRadius = 5;
    lblMsg.layer.masksToBounds = YES;
    //1.8设置label的frame
    CGFloat msgW = 200;
    CGFloat msgH = 30;
    CGFloat msgX = (self.view.frame.size.width - msgW) * 0.5;
    CGFloat msgY = (self.view.frame.size.height - msgH) * 0.5;
    lblMsg.frame = CGRectMake(msgX, msgY, msgW, msgH);
    
    
    //2.把label加到self.vie
    //因为现在使用的是UITableViewController控制器，所以self.view就是UITableView
    //当把lblMsg加到self.view中，也就是加到了UITableView中，所以当UItableView滚动的时候，lblMsg也会
    //随着滚动
    //[self.view addSubview:lblMsg];
    //[[[UIApplication sharedApplication] keyWindow]addSubview:lblMsg];//keyWindow被取代
    
    [[[[UIApplication sharedApplication] windows]firstObject]addSubview:lblMsg];
    
    
    //3.通过动画的方式慢慢显示label
    [UIView animateWithDuration:1.0 animations:^{
        //当前要执行的动画方法
        lblMsg.alpha = 0.6;
    } completion:^(BOOL finished) {
        //当动画执行完毕以后执行这里的代码
        
        //再开启一个新的动画
        [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionCurveLinear animations:^{
            lblMsg.alpha = 0;
        } completion:^(BOOL finished) {
            [lblMsg removeFromSuperview];
        }];
    }];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = 75;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - 懒加载数据
- (NSArray *)apps{
    if(_apps == nil){
        NSString *path = [[NSBundle mainBundle] pathForResource:@"apps_full.plist" ofType:nil];
        NSMutableArray *arrayModel = [NSMutableArray array];
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        for (NSDictionary *dict in array) {
            WHSApp *model = [WHSApp appWithDict:dict];
            [arrayModel addObject:model];
        }
        _apps = arrayModel;
    }
    
    return _apps;
}





#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.apps.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //获取模型数据
    WHSApp *model = self.apps[indexPath.row];
    //创建单元格（传统方式）
//    static NSString *ID = @"app_cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
//    if(cell == nil){
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
//    }
//
    
    //通过storyboard中的cell模板来加载单元格
    static NSString *ID = @"app_cell";
    WHSAppCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    //设置单元格的代理
    cell.delegate = self;
    
    
    
    
    
    //把模型设置给单元格
    cell.app = model;
    
    
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnClick:(id)sender {
    
}
    
@end
