//
//  ViewController.m
//  图片轮播器
//
//  Created by 魏华生 on 2020/2/9.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property(nonatomic,strong)NSTimer *timer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    static const int countsOfImg=5;
   static const CGFloat imgW=375;
   static const CGFloat imgH=260;
    for (int i=0; i<countsOfImg; i++) {
        UIImageView *imgView=[[UIImageView alloc]init];
        imgView.image=[UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",i+1]];
        CGFloat imgY=20;
        CGFloat imgX=i*imgW;
        imgView.frame=CGRectMake(imgX, imgY, imgW, imgH);
        
        [self.scrollView addSubview:imgView];
       
    }
    self.scrollView.contentSize=CGSizeMake(countsOfImg*imgW, 0);
    
    //实现分页0
    self.scrollView.pagingEnabled=YES;
    self.scrollView.showsHorizontalScrollIndicator=NO;
    
    self.pageControl.numberOfPages=countsOfImg;
     self.pageControl.currentPage=0;
    self.scrollView.delegate=self;
    
    // 创建定时器对象
    self.timer=[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(nextImg) userInfo:nil repeats:YES];
    
    //解决当单击界面上的其他控件时，UIScrollView停止滚动
    //原因：当前处理ui界面只有一个线程，当这个线程处理ui的拖动事件的时候就没有能力再去处理滚动操作了
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];//提升NSTimer的优先级和控件一致
    [runLoop addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
           CGFloat offSetX=scrollView.contentOffset.x+scrollView.frame.size.width*0.5;
           int pageNo=offSetX/scrollView.frame.size.width;
           self.pageControl.currentPage=pageNo;
}

-(void)nextImg{
    NSInteger page=self.pageControl.currentPage;
    if(page==self.pageControl.numberOfPages-1){
        page=0;
    }else{
        page++;
    }
    CGFloat offsetX=page*self.scrollView.frame.size.width;
    [self.scrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
}


//bug：当拖拽UIScrollView的时候，保持一段时间不松手的时候，一旦松手UIScrollView会连续滚动多次
//解决思路：在即将拖拽的时候，停止计时器，拖拽完毕后再打开一个计时器


//即将l开始拖拽
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    //停止计时器
    [self.timer invalidate];
    self.timer=nil;
    
    }
//拖拽完毕以后
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(nextImg) userInfo:nil repeats:YES];
    
    // 提升 NSTimer 的优先级与控件一致
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
    [runLoop addTimer:self.timer forMode:NSRunLoopCommonModes];
}
@end
