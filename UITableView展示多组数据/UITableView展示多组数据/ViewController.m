//
//  ViewController.m
//  UITableView展示多组数据
//
//  Created by 魏华生 on 2020/2/11.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){
        return 3;
    }else if(section==1){
        return 2;
    }else
        return 1;
    
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return @"亚洲";
    }else if(section==1){
        return @"非洲";
    }else
        return @"欧洲";
    
}   // fixed font style. use custom view (UILabel) if you want something different
- (nullable NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    if (section==0) {
         return @"亚细亚洲，日出的地方";
    }else if(section==1){
        return @"阿里菲加州，世界上最炎热的地方";
    }else
        return @"sfsdfsfdsfafa";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];

 
    if (indexPath.section==0) {
        if(indexPath.row==0){
            cell.textLabel.text=@"中国";
        }
        if(indexPath.row==1){
            cell.textLabel.text=@"日本";
        }
        if(indexPath.row==2){
            cell.textLabel.text=@"韩国";
        }
    }else if(indexPath.section==1){
        if(indexPath.row==0){
            cell.textLabel.text=@"南非";
        }
        if(indexPath.row==1){
            cell.textLabel.text=@"索马里";
        }
    }else if(indexPath.section==2){
        if(indexPath.row==0){
                   cell.textLabel.text=@"荷兰";
               }
    }
        return  cell;
}
@end
