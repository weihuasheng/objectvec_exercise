//
//  WHSGroup.m
//  UITableView的使用-汽车品牌的展示-模型套模型
//
//  Created by 魏华生 on 2020/2/14.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSGroup.h"

@implementation WHSGroup
-(instancetype)initWithDict:(NSDictionary *)dict{
    if(self=[super init]){
        
        [self setValuesForKeysWithDictionary:dict];
        
        //当有模形嵌套的时候需要手动把字典转成模型
        //创建一个用来保存模型的数组
        NSMutableArray *arrayM=[NSMutableArray array];
        //手动做一下字典转模型
        for (NSDictionary* item_dict in dict[@"cars"]) {
            WHSCar *model=[WHSCar carWithDict:item_dict];
            [arrayM addObject:model];
        }
        self.cars=arrayM;
    }
    return self;
}
+ (instancetype)GrougWithDict:(NSDictionary *)dict{
    return [[self alloc]initWithDict:dict];
}
@end
