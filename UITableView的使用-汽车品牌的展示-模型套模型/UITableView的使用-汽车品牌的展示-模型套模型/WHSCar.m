//
//  WHSCar.m
//  UITableView的使用-汽车品牌的展示-模型套模型
//
//  Created by 魏华生 on 2020/2/14.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSCar.h"

@implementation WHSCar
-(instancetype)initWithDict:(NSDictionary *)dict{
    if(self=[super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+(instancetype)carWithDict:(NSDictionary *)dict{
    return [[self alloc]initWithDict:dict];
}
@end
