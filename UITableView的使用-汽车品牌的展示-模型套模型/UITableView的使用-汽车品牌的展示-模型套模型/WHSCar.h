//
//  WHSCar.h
//  UITableView的使用-汽车品牌的展示-模型套模型
//
//  Created by 魏华生 on 2020/2/14.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSCar : NSObject
@property(nonatomic,copy)NSString *icon;
@property(nonatomic,copy)NSString *name;

-(instancetype)initWithDict:(NSDictionary*)dict;
+(instancetype)carWithDict:(NSDictionary*)dict;

@end

NS_ASSUME_NONNULL_END
