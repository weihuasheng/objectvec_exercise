//
//  WHSGroup.h
//  UITableView的使用-汽车品牌的展示-模型套模型
//
//  Created by 魏华生 on 2020/2/14.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WHSCar.h"

NS_ASSUME_NONNULL_BEGIN

@interface WHSGroup : NSObject
@property(nonatomic,strong)NSArray *cars;
@property(nonatomic,copy)NSString *title;

-(instancetype)initWithDict:(NSDictionary*)dict;
+(instancetype)GrougWithDict:(NSDictionary*)dict;
@end

NS_ASSUME_NONNULL_END
