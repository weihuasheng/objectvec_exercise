//
//  ViewController.m
//  UITableView的使用-汽车品牌的展示-模型套模型
//
//  Created by 魏华生 on 2020/2/14.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSGroup.h"

@interface ViewController ()<UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *groups;
@end

@implementation ViewController
- (NSArray *)groups{
    if(_groups==nil){
        NSString *path=[[NSBundle mainBundle]pathForResource:@"cars_total.plist" ofType:nil];
        NSArray *array=[NSArray arrayWithContentsOfFile:path];
        NSMutableArray *arrayModel=[NSMutableArray array];
        for (NSDictionary*dict in array) {
            WHSGroup *group=[WHSGroup GrougWithDict:dict];
            [arrayModel addObject:group];
        }
        _groups=arrayModel;
        
    }
    return _groups;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    WHSGroup *car=self.groups[section];
    return car.cars.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WHSGroup *group=self.groups[indexPath.section];
    WHSCar *car=group.cars[indexPath.row];
    NSString *ID=@"car_cell";
    UITableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if(cell==nil){
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    cell.imageView.image=[UIImage imageNamed:car.icon];
    cell.textLabel.text=car.name;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.groups.count;
}

- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    /**
     NSMutableArray *arrayIndex=[NSMutableArray array];
     for(WHSGroup *group in self.groups){
     [arrayIndex addObject:group.title];
     }
     return arrayIndex;
     */
    return [self.groups valueForKeyPath:@"title"];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    WHSGroup*group=self.groups[section];
    return group.title;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.dataSource=self;
}


@end
