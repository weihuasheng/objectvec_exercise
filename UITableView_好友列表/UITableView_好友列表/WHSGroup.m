//
//  WHSGroup.m
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSGroup.h"
#import "WHSFriend.h"

@implementation WHSGroup


- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self = [super init]){
        [self setValuesForKeysWithDictionary:dict];
        
        NSMutableArray *arrayM = [NSMutableArray array];
        for (NSDictionary* item_dict in dict[@"friends"]) {
            WHSFriend *model = [WHSFriend FriendWithDict:item_dict];
            [arrayM addObject:model];
        }
        self.friends = arrayM;
    }
    return self;
}

+ (instancetype)groupWithDict:(NSDictionary *)dict{
    return [[self alloc] initWithDict:dict];
}
@end
