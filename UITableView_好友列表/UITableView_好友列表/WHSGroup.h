//
//  WHSGroup.h
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSGroup : NSObject
@property (nonatomic, strong) NSArray *friends;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int online;
//表示这个组件是否可见
@property (nonatomic, assign, getter=isVisible) BOOL visible;

- (instancetype)initWithDict: (NSDictionary *)dict;
+ (instancetype)groupWithDict: (NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
