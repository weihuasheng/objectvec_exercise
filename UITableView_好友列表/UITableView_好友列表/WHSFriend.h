//
//  WHSFriend.h
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/10.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSFriend : NSObject
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *intro;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign, getter=isVip)Boolean vip;


//模型
- (instancetype)initWithDict: (NSDictionary *)dict;
+ (instancetype)FriendWithDict: (NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
