//
//  WHSFriend.m
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/10.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSFriend.h"

@implementation WHSFriend
- (instancetype)initWithDict:(NSDictionary *)dict{
    if(self =[super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)FriendWithDict:(NSDictionary *)dict{
    return [[self alloc]initWithDict:dict];
}
@end
