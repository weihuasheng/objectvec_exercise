//
//  WHSTableViewController.m
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/10.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSTableViewController.h"
#import "WHSFriend.h"
#import "WHSGroup.h"
#import "WHSFriendsCell.h"
#import "WHSHeaderFooterView.h"

@interface WHSTableViewController ()<WHSHeaderFooterViewDelegate>
@property (nonatomic, strong) NSArray *groups;
@end

@implementation WHSTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //y统一设置每一组的组标题的高度
    self.tableView.sectionHeaderHeight = 44;
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


#pragma mark - 懒加载数据
- (NSArray *)groups{
    if (_groups == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"friends.plist" ofType:nil];
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        NSMutableArray *model = [NSMutableArray array];
        for (NSDictionary *dict in array) {
            WHSGroup *group = [WHSGroup groupWithDict:dict];
            [model addObject:group];
        }
        _groups = model;
    }
    return _groups;
}

#pragma mark - headerfooterView







#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.groups.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //因为在这个方法中，要根据当前组的状态（是否展开），来设置不同的返回值
    //所以，需要为WHSGroup模型中增加一个用来保护“是否展开”状态的属性
    WHSGroup *group = self.groups[section];
    if (group.isVisible) {
         return group.friends.count;
    }else{
        return 0;
    }
    

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //1.获取模型对象（数据）
    WHSGroup *group = self.groups[indexPath.section];
    WHSFriend *friend = group.friends[indexPath.row];
    
    //2.创建单元格（视图）
    WHSFriendsCell *cell = [WHSFriendsCell friendsCellWithTableView:tableView];
    
    //3.设置单元格数据(把模型设置给单元格)
    cell.friendModel = friend;
    
    
    //4.返回单元格
    return cell;
}


//设置每一组的组标题(下面的方法设置的组标题只能是字符串，没法加图片等的其它控件)
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    WHSGroup *group = self.groups[section];
//    return group.name;
//}

//下面的方法可以放其他的控件
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //不要在这个方法直接创建一个UIView对象返回，因为这样无法实现重用该UIView
    //为了能重用每个Header中的UIView，所以这里要返回一个UITableViewHeaderFooterView
    //但是看到UITableViewHeaderFooterView的实现时，里面没有我们所要的按钮等的子控件设置，所以我们要单独写一个类型自定义
    
    //1.获取模型数据
    WHSGroup *group = self.groups[section];
    
    //2.创建一个UITableViewHeaderFooterView
    WHSHeaderFooterView *headerVw = [WHSHeaderFooterView headerFooterViewWithTableView:tableView];
    
    headerVw.tag = section;
    //3.设置数据
    headerVw.group = group;
    
    //设置headerView的代理为当前控制器
    headerVw.delegate = self;
    //4.返回view
    return headerVw;
    
}


//实现代理方法
- (void)headerFooterViewDidClickeTitleButton:(WHSHeaderFooterView *)groupHeaderView{
    //刷新tableView
    //[self.tableView reloadData];//刷新整个，不合适
    
    //局部刷新
    NSIndexSet *idxSet = [NSIndexSet indexSetWithIndex:groupHeaderView.tag];//tag就是当前第几组
    [self.tableView reloadSections:idxSet withRowAnimation:UITableViewRowAnimationFade];
    
}
@end
