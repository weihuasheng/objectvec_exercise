//
//  WHSHeaderFooterView.m
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSHeaderFooterView.h"
#import "WHSGroup.h"
@interface WHSHeaderFooterView()
@property (nonatomic, weak) UIButton *btnGroupTitle;
@property (nonatomic, weak) UILabel *lblCount;

@end

@implementation WHSHeaderFooterView

+ (instancetype)headerFooterViewWithTableView:(UITableView *)tableView{
    static NSString *ID = @"group_header_view";
      WHSHeaderFooterView *headerVw = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
      if (headerVw == nil) {
          headerVw = [[WHSHeaderFooterView alloc]initWithReuseIdentifier:ID];
          //因为想在创建headerVw的时候里面就有一个按钮，一个label，所以需要重写initWithReuseIdentifier方法
      }
    return headerVw;
}

//只是有了控件，还没有设置数据,重写initWithReuseIdentifier方法，创建headerVw的时候，同时创建子控件
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        //创建按钮
        UIButton *btnGroupTitle = [[UIButton alloc] init];
        //设置按钮图片
        [btnGroupTitle setImage:[UIImage imageNamed:@"buddy_header_arrow"] forState:UIControlStateNormal];
        //设置按钮的文字颜色
        [btnGroupTitle setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //设置按钮默认的背景图片和高亮状态的背景图片
        [btnGroupTitle setBackgroundImage:[UIImage imageNamed:@"buddy_header_bg"] forState:UIControlStateNormal];
        [btnGroupTitle setBackgroundImage:[UIImage imageNamed:@"buddy_header_bg_highlighted"] forState:UIControlStateHighlighted];
        //设置按钮中内容整体右对齐
        btnGroupTitle.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //设置按钮的内容的内边距
        btnGroupTitle.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        //设置按钮标题距离左边图片的边距
        btnGroupTitle.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        //为按钮添加事件
        [btnGroupTitle addTarget:self action:@selector(btnGroupTitleClicked) forControlEvents:UIControlEventTouchUpInside];
        
        //设置按钮中图片的显示方式（不要让图片在进行旋转的时候进行拉伸）
        btnGroupTitle.imageView.contentMode = UIViewContentModeCenter;
        //设置图片框超出部分不要截掉
        btnGroupTitle.imageView.clipsToBounds = NO;
        
        
        [self.contentView addSubview:btnGroupTitle];
        self.btnGroupTitle = btnGroupTitle;
        //创建label
        UILabel *lblCount =[[UILabel alloc] init];
        [self.contentView addSubview:lblCount];
        self.lblCount = lblCount;
    }
    return self;
}

//组标题按钮的点击事件
-(void) btnGroupTitleClicked{
    //1.设置状态
    self.group.visible = !self.group.visible;
    
    //2.刷新tableview
    //通过代理实现
    if ([self.delegate respondsToSelector:@selector(headerFooterViewDidClickeTitleButton:)]) {
        //调用代理方法
        [self.delegate headerFooterViewDidClickeTitleButton:self];
    }
    

    
}

//当一个新的header view已经加到某个父控件中的时候执行这个方法
- (void)didMoveToSuperview{
    if (self.group.isVisible) {
         //3.实现按钮的图片的旋转
           self.btnGroupTitle.imageView.transform = CGAffineTransformMakeRotation(M_PI_2);//旋转90度
    }else{
            self.btnGroupTitle.imageView.transform = CGAffineTransformMakeRotation(0);
    }
   
}


//给控件设置数据
- (void)setGroup:(WHSGroup *)group{
    _group = group;
    //设置数据
    //因为按钮上的左边的图片都是一样的，没必要放在这里，可以放在initWithReuseIdentifier,创建headerView的同时就有了图片
    //设置按钮上的文字
    [self.btnGroupTitle setTitle:group.name forState:UIControlStateNormal];
    
    //设置label上的文字
    self.lblCount.text = [NSString stringWithFormat:@"%d / %lu", group.online,(unsigned long)group.friends.count ];
    
    //设置frame不要写在这里，因为这里获取的当前控件（self）的宽和高都是0
    
    //设置按钮中的图片旋转问题（防止我用的是已经重用的cell的图片不一致，不如：某一组的没有展开，但图片却向下的问题，原因是用了重用的cell）
    if (self.group.isVisible) {
          //3.实现按钮的图片的旋转
            self.btnGroupTitle.imageView.transform = CGAffineTransformMakeRotation(M_PI_2);//旋转90度
     }else{
             self.btnGroupTitle.imageView.transform = CGAffineTransformMakeRotation(0);
     }
    
    
    
}

//设置数据，写在set方法，frame写在下面的方面中
//当前控件发生改变的时候会调用这个方法
- (void)layoutSubviews{
    [super layoutSubviews];
    
    //设置按钮的frame
    self.btnGroupTitle.frame = self.bounds;//当前view（self）的bounds（width，height）赋值给按钮的frame
    
    //设置lable的frame
    CGFloat lblW = 100;
    CGFloat lblH = self.bounds.size.height;
    CGFloat lblX = self.bounds.size.width - 10 - lblW;
    CGFloat lblY = 0;
    self.lblCount.frame = CGRectMake(lblX, lblY, lblW, lblH);
    
}
@end
