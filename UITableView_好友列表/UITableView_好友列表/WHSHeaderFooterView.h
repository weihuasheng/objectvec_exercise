//
//  WHSHeaderFooterView.h
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WHSGroup.h"

NS_ASSUME_NONNULL_BEGIN
@class WHSHeaderFooterView;
@protocol WHSHeaderFooterViewDelegate <NSObject>

- (void)headerFooterViewDidClickeTitleButton:(WHSHeaderFooterView *)groupHeaderView;

@end
@class WHSGroup;
@interface WHSHeaderFooterView : UITableViewHeaderFooterView
@property (nonatomic, strong) WHSGroup *group;


+ (instancetype)headerFooterViewWithTableView:(UITableView *)tableView;

//增加一个代理属性

@property (nonatomic, weak) id<WHSHeaderFooterViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
