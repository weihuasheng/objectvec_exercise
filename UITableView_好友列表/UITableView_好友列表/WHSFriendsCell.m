//
//  WHSFriendsCell.m
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSFriendsCell.h"
#import "WHSFriend.h"
@interface WHSFriendsCell()


@end

@implementation WHSFriendsCell



+ (instancetype)friendsCellWithTableView:(UITableView *)tableView{
     static NSString *ID = @"friend_cell";
     WHSFriendsCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
     if (cell == nil) {
         cell = [[WHSFriendsCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
     }
    return cell;
}

- (void)setFriendModel:(WHSFriend *)friendModel{
    _friendModel = friendModel;
    //把模型中的数据设置给单元格的子控件
    self.imageView.image = [UIImage imageNamed:friendModel.icon];
    self.textLabel.text = friendModel.name;
    self.detailTextLabel.text = friendModel.intro;
    
    //设置当前的好友是不是vip来决定昵称是否为红色
    self.textLabel.textColor = friendModel.isVip ? [UIColor redColor] : [UIColor blackColor];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
