//
//  WHSFriendsCell.h
//  UITableView_好友列表
//
//  Created by 魏华生 on 2020/3/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WHSFriend.h"

NS_ASSUME_NONNULL_BEGIN
@class WHSFriend;
@interface WHSFriendsCell : UITableViewCell

@property (nonatomic, strong)WHSFriend *friendModel;

+ (instancetype)friendsCellWithTableView: (UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
