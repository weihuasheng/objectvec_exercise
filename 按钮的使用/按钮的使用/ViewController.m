//
//  ViewController.m
//  按钮的使用
//
//  Created by 魏华生 on 2020/1/18.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btn;
- (IBAction)up;
- (IBAction)down;
- (IBAction)right;
- (IBAction)left;
- (IBAction)plus;
- (IBAction)minu;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)up {
    //NSLog(@"up");
    //获取原始的frame
    CGRect originFrame=self.btn.frame;
    //修改frame
    originFrame.origin.y -=10;
    //重新赋值
    self.btn.frame=originFrame;
    
    //如果点击没有反应，则可能是自动布局，取消即可
}

- (IBAction)down {
     //NSLog(@"down");
    CGRect originFrame=self.btn.frame;
     originFrame.origin.y +=10;
     self.btn.frame=originFrame;
}

- (IBAction)right {
     //NSLog(@"right");
    CGRect originFrame=self.btn.frame;
     originFrame.origin.x +=10;
     self.btn.frame=originFrame;
}

- (IBAction)left {
     //NSLog(@"left");
    CGRect originFrame=self.btn.frame;
     originFrame.origin.x -=10;
     self.btn.frame=originFrame;
}

- (IBAction)plus {
     //NSLog(@"plus");
    CGRect originFrame=self.btn.frame;
    originFrame.size.height +=10;
    originFrame.size.width  +=10;
    self.btn.frame=originFrame;
}

- (IBAction)minu {
   // NSLog(@"minu");
    CGRect originFrame=self.btn.frame;
    originFrame.size.height -=10;
    originFrame.size.width  -=10;
    self.btn.frame=originFrame;
}
@end
