//
//  WHSGroup.m
//  展示汽车品牌
//
//  Created by 魏华生 on 2020/2/11.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSGroup.h"

@implementation WHSGroup
-(instancetype)initWithDict:(NSDictionary *)dict{
    if(self=[super init]){
        self.cars=dict[@"cars"];
        self.title=dict[@"title"];
        self.desc=dict[@"desc"];
    }
    return self;
}
+(instancetype)groupWithDict:(NSDictionary*)dict{
    return [[self alloc]initWithDict:dict];
}
@end
