//
//  ViewController.m
//  展示汽车品牌
//
//  Created by 魏华生 on 2020/2/11.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSGroup.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSArray *car;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ViewController
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.car.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    WHSGroup *car=self.car[section];
    return car.cars.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    WHSGroup *car=self.car[indexPath.section];
    NSString *brand=car.cars[indexPath.row];
    cell.textLabel.text=brand;
    return cell;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    WHSGroup *car=self.car[section];
    return car.title;
} // fixed font style. use custom view (UILabel) if you want something different
- (nullable NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    WHSGroup *car=self.car[section];
    return car.desc;
}
-(NSArray *)car{
    if (_car==nil) {
       NSString*path= [[NSBundle mainBundle]pathForResource:@"cars_simple.plist" ofType:nil];
        NSMutableArray *model=[NSMutableArray array];
        NSArray *array=[NSArray arrayWithContentsOfFile:path];
        for (NSDictionary *dict in array) {
            WHSGroup *group=[WHSGroup groupWithDict:dict];
            [model addObject:group];
        }
        _car=model;
    }
    return _car;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
}


@end
