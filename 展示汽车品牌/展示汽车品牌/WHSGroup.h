//
//  WHSGroup.h
//  展示汽车品牌
//
//  Created by 魏华生 on 2020/2/11.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSGroup : NSObject
  @property(nonatomic,strong)  NSArray *cars;
   @property(nonatomic,copy) NSString *title;
   @property(nonatomic,strong) NSString *desc;

-(instancetype)initWithDict:(NSDictionary *)dict;
+(instancetype)groupWithDict:(NSDictionary*)dict;
@end

NS_ASSUME_NONNULL_END
