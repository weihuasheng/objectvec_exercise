//
//  SceneDelegate.h
//  展示汽车品牌
//
//  Created by 魏华生 on 2020/2/11.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

