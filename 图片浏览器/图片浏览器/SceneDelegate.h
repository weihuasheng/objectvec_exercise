//
//  SceneDelegate.h
//  图片浏览器
//
//  Created by 魏华生 on 2020/1/27.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

