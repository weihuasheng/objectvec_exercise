//
//  ViewController.m
//  图片浏览器
//
//  Created by 魏华生 on 2020/1/27.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) NSArray *pic;



// 自己写一个索引， 来控制当前显示的是第几张图片
// 这个属性一开始没有赋值就是0
@property (nonatomic, assign) int indexOfphoto;
- (IBAction)up;
- (IBAction)next;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *upBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.indexOfphoto=-1;
    [self next];
}
-(NSArray *)pic{
    if(_pic== nil){
        // 写代码加载pic.plist文件中的数据到_pic
        // 1. 获取pic.plist文件的路径
        // 获取pic.plist文件的路径赋值给path变量
        // [NSBundle mainBundle]表示获取这个app安装到手机上时的根目录
        // 然后在app的安装的根目录下搜索pic.plist文件的路径
        NSString *path=[[NSBundle mainBundle] pathForResource:@"photoList.plist" ofType:nil];
        NSArray *temp=[NSArray arrayWithContentsOfFile:path];
        _pic=temp;
    }
    return _pic;
}

- (IBAction)up {
    self.indexOfphoto --;
    [self setData];
    
}

- (IBAction)next {
    self.indexOfphoto ++;
    [self setData];
}
-(void)setData{
    NSDictionary *dict=self.pic[self.indexOfphoto];
    self.labelTitle.text=[NSString stringWithFormat:@"%d/%ld",self.indexOfphoto +1,self.pic.count];
    self.imageView.image=[UIImage imageNamed:dict[@"icon"]];
    self.labelTitle.text=dict[@"title"];
    
    self.upBtn.enabled=(self.indexOfphoto !=0);
    self.nextBtn.enabled=(self.indexOfphoto !=(self.pic.count -1));
}
@end
