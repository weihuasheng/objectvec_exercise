//
//  WHSQuestion.h
//  超级猜图
//
//  Created by 魏华生 on 2020/2/2.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSQuestion : NSObject
@property(nonatomic,copy)NSString *answer;
@property(nonatomic,copy)NSString *icon;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,strong)NSArray *options;

-(instancetype)initWithDict:(NSDictionary *) dict;
+(instancetype)questionWithDict:(NSDictionary *) dict;


@end

NS_ASSUME_NONNULL_END
