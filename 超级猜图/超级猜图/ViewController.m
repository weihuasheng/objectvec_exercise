//
//  ViewController.m
//  超级猜图
//
//  Created by 魏华生 on 2020/2/1.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSQuestion.h"

@interface ViewController ()
@property(nonatomic,strong)NSArray *questions;
@property(nonatomic,assign)int index;//索引控制图片的跳转
@property (weak, nonatomic) IBOutlet UILabel *lblIndex;
@property (weak, nonatomic) IBOutlet UIButton *btnScore;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property(nonatomic,assign)CGRect iconFrame;
@property(nonatomic,weak)UIButton *cover;
@property (weak, nonatomic) IBOutlet UIView *answerView;
- (IBAction)btnNextClick;
- (IBAction)bigPicture;
- (IBAction)btnIconClick:(id)sender;

@end

@implementation ViewController
//重写方法，使状态栏变为浅色
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;//枚举类型
}

//懒加载
-(NSArray *)questions{
    if(_questions==nil){
        NSString *path=[[NSBundle mainBundle]pathForResource:@"questions.plist" ofType:nil ];
        NSArray *array=[NSArray arrayWithContentsOfFile:path];
        NSMutableArray *arrayModels=[NSMutableArray array];
        for (NSDictionary *dict in array) {
            WHSQuestion *model=[WHSQuestion questionWithDict:dict];
            [arrayModels addObject:model];
        }
         _questions=arrayModels;
    }
    return _questions;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //当页面显示起起来时，能够正常显示第一张图片
    self.index=-1;
    [self btnNextClick];
    
}


- (IBAction)btnNextClick {
    [self nextQuestion];
    
}

- (IBAction)bigPicture {
    CGFloat originwidth=self.view.frame.size.width;
    CGFloat originHeight=self.view.frame.size.height;
    UIButton *btnBack=[[UIButton alloc]init];
    btnBack.backgroundColor=[UIColor blackColor];
    btnBack.alpha=0.0;
    btnBack.frame=CGRectMake(0, 0, originwidth, originHeight);
    [self.view addSubview:btnBack];
    [btnBack addTarget:self action:@selector(smallPicture) forControlEvents:UIControlEventTouchUpInside];
    CGFloat bigWidth=originwidth;
    CGFloat bigHeight=bigWidth;
    CGFloat bigX=0;
    CGFloat bigY=(originHeight-bigHeight)*0.5;
    self.iconFrame=self.btnIcon.frame;
    self.cover=btnBack;
    [UIView animateWithDuration:1 animations:^{
        [self.view bringSubviewToFront:self.btnIcon];
        self.btnIcon.frame=CGRectMake(bigX, bigY, bigWidth, bigHeight);
        btnBack.alpha=0.6;
    }];
    

    
}

- (IBAction)btnIconClick:(id)sender {
    if (self.cover==nil) {
        [self bigPicture];
    }else{
        [self smallPicture];
    }
}

-(void)smallPicture{
    [UIView animateWithDuration:1 animations:^{
        self.btnIcon.frame=self.iconFrame;
        [self.cover removeFromSuperview];
    }];

}

-(void)nextQuestion{
    self.index++;
    WHSQuestion *model=self.questions[self.index];
    //设置索引文字
    self.lblIndex.text=[NSString stringWithFormat:@"%d/%ld",(self.index +1),self.questions.count ];
    //设置图片
    [self.btnIcon setImage:[UIImage imageNamed:model.icon] forState:UIControlStateNormal];
    //设置标题
    self.lblTitle.text=model.title;
    //设置当显示完最后一张图片时，按钮不可点击
    if (self.index==self.questions.count-1) {
        self.btnNext.enabled=NO;//表示按钮不可用
    }
    
    
    NSInteger count=model.answer.length;
    for(int i=0;i<count;i++){
        
    }
}
@end
