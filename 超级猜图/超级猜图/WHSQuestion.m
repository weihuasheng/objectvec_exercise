//
//  WHSQuestion.m
//  超级猜图
//
//  Created by 魏华生 on 2020/2/2.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSQuestion.h"

@implementation WHSQuestion

-(instancetype)initWithDict:(NSDictionary *)dict{
    if (self=[super init]) {
        self.answer=dict[@"answer"];
        self.icon=dict[@"icon"];
        self.title=dict[@"title"];
        self.options=dict[@"options"];
    }
    return self;
}
+(instancetype)questionWithDict:(NSDictionary *)dict{
    return [[self alloc]initWithDict:dict];
}
@end
