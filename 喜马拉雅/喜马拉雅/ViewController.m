//
//  ViewController.m
//  喜马拉雅
//
//  Created by 魏华生 on 2020/2/7.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *lastImg;
@property (weak, nonatomic) IBOutlet UIImageView *firstImg;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGFloat scrollWidth=self.scrollView.frame.size.width;
    CGFloat scrollHeight=CGRectGetMaxY(self.lastImg.frame);
    self.scrollView.contentInset=UIEdgeInsetsMake(95, 0, 0, 0);
     self.scrollView.contentOffset = CGPointMake(0, -74);
    self.scrollView.contentSize=CGSizeMake(scrollWidth, scrollHeight);
    
    //设置代理事件
    self.scrollView.delegate=self;
    
}

//实现协议中的方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSLog(@"正在滚");
}


@end
