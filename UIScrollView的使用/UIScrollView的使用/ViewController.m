//
//  ViewController.m
//  UIScrollView的使用
//
//  Created by 魏华生 on 2020/2/7.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.scrollView.contentSize=self.imgView.image.size;
    self.scrollView.contentInset=UIEdgeInsetsMake(20, 20, 50, 35);
    
}


@end
