//
//  ViewController.m
//  对按钮的使用进行改写
//
//  Created by 魏华生 on 2020/1/21.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)move:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *pitcure;
- (IBAction)change:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)move:(id)sender {
    CGRect originFrame=self.pitcure.frame;
    if([sender tag]==10){//在新的版本里不能直接sender.tag
        originFrame.origin.y -=10;
        self.pitcure.frame=originFrame;
    }
    else if([sender tag]==11){
           originFrame.origin.x +=10;
           self.pitcure.frame=originFrame;
       }
    else if([sender tag]==12){
           originFrame.origin.y +=10;
           self.pitcure.frame=originFrame;
       }
    else if([sender tag]==13){
           originFrame.origin.x -=10;
           self.pitcure.frame=originFrame;
       }
    
}
- (IBAction)change:(UIButton *)sender {
    CGRect originFrame=self.pitcure.frame;
    if ([sender tag]==14){
        originFrame.size.height +=10;
        originFrame.size.width +=10;
        self.pitcure.frame=originFrame;
    }
    else if ([sender tag]==15){
        originFrame.size.height -=10;
        originFrame.size.width -=10;
        self.pitcure.frame=originFrame;
    }
}
@end
