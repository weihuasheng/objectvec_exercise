//
//  NSString+NumberConvenience.h
//  category_test
//
//  Created by student_weihuasheng on 2019/12/7.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <AppKit/AppKit.h>


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (NumberConvenience)
-(NSNumber *) lengthAsNumber;
@end

NS_ASSUME_NONNULL_END
