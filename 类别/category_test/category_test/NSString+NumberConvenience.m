//
//  NSString+NumberConvenience.m
//  category_test
//
//  Created by student_weihuasheng on 2019/12/7.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import "NSString+NumberConvenience.h"

#import <AppKit/AppKit.h>


@implementation NSString (NumberConvenience)
-(NSNumber *) lengthAsNumber{
    NSUInteger length=[self length];
    return ([NSNumber numberWithUnsignedInt:length]);
}

@end
