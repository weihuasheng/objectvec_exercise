//
//  main.m
//  category_test
//
//  Created by student_weihuasheng on 2019/12/7.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+NumberConvenience.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        //NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:[@"hello" lengthAsNumber] forKey:@"hello"];
        
        [dict setObject:[@"iLikeFish" lengthAsNumber] forKey:@"iLikeFish"];
        
        [dict setObject:[@"Once upon a time" lengthAsNumber] forKey:@"Once upon a time"];
        
        NSLog(@"%@",dict);
    }
    return 0;
}
