//
//  main.m
//  catagory
//
//  Created by student_weihuasheng on 2019/12/9.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CatagoryThing.h"
#import "CatagoryThing+Thing1.h"
#import "CatagoryThing+Thing2.h"
#import "CatagoryThing+Thing3.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        CatagoryThing *thing=[[CatagoryThing alloc]init];
        [thing setThing1:5];
         [thing setThing2:23];
         [thing setThing3:42];
        NSLog(@"Things are %@",thing);
        
    }
    return 0;
}
