//
//  CatagoryThing+Thing2.m
//  catagory
//
//  Created by student_weihuasheng on 2019/12/9.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import "CatagoryThing+Thing2.h"

#import <AppKit/AppKit.h>


@implementation CatagoryThing (Thing2)
- (void)setThing2:(NSInteger)t2{
    thing2=t2;
}
-(NSInteger)thing2{
    return thing2;
}
@end
