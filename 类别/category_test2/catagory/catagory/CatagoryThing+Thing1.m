//
//  CatagoryThing+Thing1.m
//  catagory
//
//  Created by student_weihuasheng on 2019/12/9.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import "CatagoryThing+Thing1.h"

#import <AppKit/AppKit.h>


@implementation CatagoryThing (Thing1)
- (void)setThing1:(NSInteger)t1{
    thing1=t1;
}
-(NSInteger)thing1{
    return thing1;
}
@end
