//
//  CatagoryThing+Thing3.m
//  catagory
//
//  Created by student_weihuasheng on 2019/12/9.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import "CatagoryThing+Thing3.h"

#import <AppKit/AppKit.h>


@implementation CatagoryThing (Thing3)
- (void)setThing3:(NSInteger)t3{
    thing3=t3;
}
-(NSInteger)thing3{
    return thing3;
}
@end
