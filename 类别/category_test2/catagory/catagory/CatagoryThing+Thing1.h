//
//  CatagoryThing+Thing1.h
//  catagory
//
//  Created by student_weihuasheng on 2019/12/9.
//  Copyright © 2019 student_weihuasheng. All rights reserved.
//

#import <AppKit/AppKit.h>


#import "CatagoryThing.h"

NS_ASSUME_NONNULL_BEGIN

@interface CatagoryThing (Thing1)
- (void)setThing1:(NSInteger)thing1;
-(NSInteger)thing1;
@end

NS_ASSUME_NONNULL_END
