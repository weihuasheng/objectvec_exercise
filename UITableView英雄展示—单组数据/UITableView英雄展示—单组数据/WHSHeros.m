//
//  WHSHeros.m
//  UITableView英雄展示—单组数据
//
//  Created by 魏华生 on 2020/2/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "WHSHeros.h"

@implementation WHSHeros
-(instancetype)initWithDict:(NSDictionary*)dict{
    if(self=[super init]){
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}
+(instancetype)HerosWithDict:(NSDictionary*)dict{
    return [[self alloc]initWithDict:dict];
}
@end
