//
//  ViewController.m
//  UITableView英雄展示—单组数据
//
//  Created by 魏华生 on 2020/2/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import "ViewController.h"
#import "WHSHeros.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *arrays;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,assign)NSInteger index;
@end

@implementation ViewController
- (NSArray *)arrays{
    if(_arrays==nil){
        NSString *path=[[NSBundle mainBundle]pathForResource:@"heros.plist" ofType:nil];
        NSMutableArray *arrayM=[NSMutableArray array];
        NSArray *array=[NSArray arrayWithContentsOfFile:path];
        for (NSDictionary*dict in array) {
            WHSHeros *heros=[WHSHeros HerosWithDict:dict];
            [arrayM addObject:heros];
        }
        _arrays=arrayM;
    }
    return _arrays;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    
    return self.arrays.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger sun=indexPath.row;
    if(sun%2==0){
        return 60;
    }else{
        return 120;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    WHSHeros* heros=self.arrays[indexPath.row];
    //cell的重用，优化性能
    static NSString* ID=@"hero_cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:ID];
    if(cell==nil){
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
    
    cell.imageView.image=[UIImage imageNamed:heros.icon];
    cell.textLabel.text=heros.name;
    cell.detailTextLabel.text=heros.intro;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // 获取当前被选中的这行的英雄的名称
    WHSHeros *hero = self.arrays[indexPath.row];
    
    
    
    // 创建一个对话框对象
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"编辑英雄" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//
//    // 把当前行的索引保存到alertview的Tag中
//    alertView.tag = indexPath.row;
//
//    // 修改UIAlertViwe的样式, 显示出一个文本框来
//    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
//
//    // 获取那个文本框, 并且设置文本框中的文字为hero.name
//    [alertView textFieldAtIndex:0].text = hero.name;
//
//    // 显示对话框
//    [alertView show];
    
    UIAlertController *alertView=[UIAlertController alertControllerWithTitle:@"编辑英雄" message:nil preferredStyle:UIAlertControllerStyleAlert];
    self.index=indexPath.row;
    UIAlertAction *canceButton=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *otherButton=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
              // 1. 获取用户文本框中输入的内容
             UITextField *textField= alertView.textFields.firstObject;
             NSString* userIn=textField.text;
             
             
             // 2. 找到对应的英雄模型
             WHSHeros *hero = self.arrays[self.index];
             
             
             // 3. 修改英雄模型的name
             hero.name = userIn;
             
             // 4. 刷新tableView(重新刷新数据的意思就是重新调用UITableView的数据源对象中的那些数"据源方法")
             // reloadData表示刷新整个tableView
             //[self.tableView reloadData]; // 重新刷新table view
             
             // 局部刷新, 刷新指定的行
             // 创建一个行对象
             NSIndexPath *idxPath = [NSIndexPath indexPathForRow:self.index inSection:0];
             [self.tableView reloadRowsAtIndexPaths:@[idxPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
    [alertView addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text=hero.name;
    }];
    
    [alertView addAction:canceButton];
    [alertView addAction:otherButton];
    [self presentViewController:alertView animated:YES completion:nil];
    
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}
@end
