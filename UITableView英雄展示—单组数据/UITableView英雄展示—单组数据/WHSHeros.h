//
//  WHSHeros.h
//  UITableView英雄展示—单组数据
//
//  Created by 魏华生 on 2020/2/13.
//  Copyright © 2020 魏华生. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WHSHeros : NSObject
@property(nonatomic,copy)NSString *icon;
@property(nonatomic,copy)NSString *intro;
@property(nonatomic,copy)NSString *name;

-(instancetype)initWithDict:(NSDictionary*)dict;
+(instancetype)HerosWithDict:(NSDictionary*)dict;
@end

NS_ASSUME_NONNULL_END
